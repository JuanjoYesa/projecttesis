from django.apps import AppConfig


class VercapasConfig(AppConfig):
    name = 'vercapas'
