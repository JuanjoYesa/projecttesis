from django.db import models
from django.contrib.gis.db import models

class Pixeles(models.Model):
    #---Atributos del Shape-------
    id_pix = models.BigIntegerField(primary_key=True)
    x_min = models.FloatField()
    x_max = models.FloatField()
    y_min = models.FloatField()
    y_max = models.FloatField()
    #---Atributos de lat y lon-------
    indx_lat = models.IntegerField(null=True)
    indx_lon = models.IntegerField(null=True)
    #--Boolean si afecta o no a la cuenca
    es_mbc = models.BooleanField(default=False)
    
    areakm = models.BigIntegerField()
    
    mpoly = models.MultiPolygonField(srid=4326)

    class Meta:
        db_table= 'pixeles'

class DatosDesc(models.Model):
    id_arch = models.AutoField(primary_key=True)
    starthour = models.TextField(blank=True, null=True)
    endhour = models.TextField(blank=True, null=True)
    code = models.TextField(blank=True, null=True)
    dofyear = models.TextField(blank=True, null=True)

    class Meta:
        db_table= 'datos_desc'

class PrecipData(models.Model):
    id_pd = models.AutoField(primary_key=True)
    id_pix = models.ForeignKey(Pixeles, models.DO_NOTHING, db_column='id_pix')
    id_arch = models.ForeignKey(DatosDesc, models.DO_NOTHING, db_column='id_arch')
    fecha = models.DateTimeField(blank=True, null=True)
    mm_h = models.FloatField(blank=True, null=True)
    vol_total = models.FloatField(blank=True, null=True)
    
    class Meta:
        db_table= 'precip_data'

    def __string__(self):
        return self.fecha

    def natural_key(self):
        return self.pixeles.natural_key() + (self.mm_h,)

class DatosAtmos(models.Model):
    id_da = models.AutoField(primary_key=True)
    fecha_datmos = models.DateTimeField(blank=True, null=True)
    temperatura = models.FloatField(blank=True, null=True)
    humedad = models.FloatField(blank=True, null=True)
    presion = models.FloatField(blank=True, null=True)

    class Meta:
        db_table = 'datosatmos'

class SubCuencas(models.Model):
    comid = models.CharField(max_length = 20)
    grid_code = models.CharField(max_length = 20)
    grid_count = models.CharField(max_length = 20)
    prod_unit = models.CharField(max_length = 20)
    areasqkm = models.FloatField()
    mpoly = models.MultiPolygonField(srid=4326)

    def __unicode__(self):
        return self.comid

    class Meta:
        db_table= 'sub_cuencas'

class SegCuencas(models.Model):
    id_seg = models.AutoField(primary_key=True)
    id_pix = models.ForeignKey(Pixeles, models.DO_NOTHING, db_column='id_pix', blank=True, null=True)
    id_subc = models.ForeignKey('SubCuencas', models.DO_NOTHING, db_column='id_subc')
    area_cobxpix = models.FloatField(blank=True, null=True)

    class Meta:
        db_table= 'seg_cuencas'

class PronostPrecip(models.Model):
    id_pprecip = models.AutoField(primary_key=True)
    id_pd = models.ForeignKey('PrecipData', models.DO_NOTHING, db_column='id_pd')
    fecha_pprecip = models.DateTimeField(blank=True, null=True)
    valor_pprecip = models.FloatField(blank=True, null=True)

    class Meta:
        db_table='pronost_precip'

class PronostDAtmos(models.Model):
    id_pdatmos = models.AutoField(primary_key=True)
    id_pd = models.ForeignKey('PrecipData', models.DO_NOTHING, db_column='id_pd', null=True)
    fecha_pdatmos = models.DateTimeField(blank=True, null=True)
    valor_ptemp = models.FloatField(blank=True, null=True)
    valor_phumd = models.FloatField(blank=True, null=True)
    valor_ppresion = models.FloatField(blank=True, null=True)

    class Meta:
        db_table='pronost_datmos'


class DataHistSegsubc(models.Model):
    id_datsegm = models.AutoField(primary_key=True)
    id_seg = models.ForeignKey('SegCuencas', models.DO_NOTHING, db_column='id_seg')
    fecha_segsubc = models.DateTimeField(blank=True, null=True)
    vol_segm = models.FloatField(blank=True, null=True)
    
    class Meta:
        db_table= 'data_hist_segsubc'

class DataVolAcumSubcuencas(models.Model):
    id_datavolacum = models.AutoField(primary_key=True)
    id_subc = models.ForeignKey('SubCuencas', models.DO_NOTHING, db_column='id_subc')
    fecha_subc = models.DateTimeField(blank=True,null=True)
    vol_acum = models.FloatField(blank=True, null=True)
    
    def __unicode__(self):
        return self.id_datavolacum

    class Meta:
        db_table= 'data_vol_acum_subcuencas'

class DataHistSubc(models.Model):
    id_data_sub = models.AutoField(primary_key=True)
    id_subc = models.ForeignKey('SubCuencas', models.DO_NOTHING, db_column='id_subc')
    fecha_subc = models.DateTimeField(blank=True, null=True)
    vol_subc = models.FloatField(blank=True, null=True)
    
    class Meta:
        db_table= 'data_hist_subc'