import os
from django.contrib.gis.utils import LayerMapping
from .models import CuencaMboiCae

cuenca_mapping = {
    'comid' : 'COMID',
    'grid_code' : 'GRID_CODE',
    'grid_count' : 'GRID_COUNT',
    'prod_unit' : 'PROD_UNIT',
    'areasqkm' : 'AREASQKM',

    'mpoly' : 'MULTIPOLYGON',
}

cuenca_shp = os.path.abspath(
    os.path.join(os.path.dirname(__file__), 'shapes', 'Cuenca MboiCae2.shp'),
)

def run(verbose=True):
    lm = LayerMapping(CuencaMboiCae, cuenca_shp, cuenca_mapping, transform=False)
    lm.save(strict=True, verbose=verbose)