from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.http import HttpResponse
from django.http import JsonResponse
from django.views import generic
from django.views.generic import TemplateView
from django.core.serializers import serialize 
from django.db import connection
from .models import Pixeles, SubCuencas, PrecipData, DataHistSubc, DataVolAcumSubcuencas, PronostPrecip, SegCuencas
from .forms import XDSoftEventForm
from datetime import datetime, timedelta
from pytz import timezone
import json

def index(request):

	return render(
		request,
		'index.html'
		)

def PixelesData(request):
	pixeles = serialize('geojson', Pixeles.objects.all(), indent=2)
	return HttpResponse(pixeles, content_type='json')

def SubCuencasData(request):
	subcuencas = serialize('geojson', SubCuencas.objects.all())
	return HttpResponse(subcuencas, content_type='geojson')

fecha_obserbada='2018-03-15 13:00:00'

def Rain_on_Pixel(request):
	
	pixeles = Pixeles.objects.all()
	#---Se cargan los valores de los pixeles en listas individuales------------------	
	pix_id = pixeles.values_list('id_pix', flat=True)
	x_mi = pixeles.values_list('x_min', flat=True)
	x_ma = pixeles.values_list('x_max', flat=True)
	y_mi = pixeles.values_list('y_min', flat=True)
	y_ma = pixeles.values_list('y_max', flat=True)
	ind_lat = pixeles.values_list('indx_lat', flat=True)
	ind_lon = pixeles.values_list('indx_lon', flat=True)
	b_mbc = pixeles.values_list('es_mbc', flat=True)
	areak = pixeles.values_list('areakm', flat=True)

	#valores = PrecipData.objects.filter(fecha=fecha_obserbada).order_by('id_pix')
	valores = PrecipData.objects.order_by('-fecha', 'id_pix')[:30]
	#---Se cargan los valores de los precipitación en listas individuales------------------	
	date = valores.values_list('fecha', flat=True)
	cant = valores.values_list('mm_h', flat=True)
	volumen = valores.values_list('vol_total', flat=True)

	geo_json = [{
				  "type": "Feature",
				  "properties": {
					"x_min": x_min,
					"x_max": x_max,
					"y_min": y_min,
					"y_max": y_max,
					"indx_lat": indx_lat,
					"indx_lon": indx_lon,
					"es_mbc": es_mbc,
					"areakm": areakm,
					"pk": id_pix,
					"values":{
						"fecha": fecha,
						"mm_h": mm_h,
						"vol_total": vol_total
					}
				  },
				  "geometry": {
					"type": "MultiPolygon",
					"coordinates": [
					  [
						[
						  [
							x_min,
							y_max
						  ],
						  [
							x_max,
							y_max
						  ],
						  [
							x_max,
							y_min
						  ],
						  [
							x_min,
							y_min
						  ],
						  [
							x_min,
							y_max
						  ]
						]
					  ]
					]
				  }
				}
				for id_pix, x_min, x_max, y_min, y_max, indx_lon, indx_lat, es_mbc, areakm, fecha, mm_h, vol_total in zip(pix_id, x_mi, x_ma, y_mi, y_ma, ind_lon, ind_lat, b_mbc, areak, date, cant, volumen)]
	georesp = JsonResponse({"type": "FeatureCollection", "crs": {"type": "name", "properties": {"name": "EPSG:4326"}}, "features": geo_json}, safe=False)
	return HttpResponse(georesp, content_type='geojson')

def Precip_Data(request):
	datos_lluvia = PrecipData.objects.order_by('-fecha', 'id_pix')[:30]
	resp = serialize('json', datos_lluvia, indent=2)
	return HttpResponse(resp, content_type='json')

def last_update(request):
	last_date = PrecipData.objects.order_by('-fecha')[:1]#DataVolAcumSubcuencas.objects.order_by('-fecha_subc')[:1]
	resp = serialize('json', last_date, indent=2)
	return HttpResponse(resp, content_type='json')

def watercatchment (request):
	subcuencas = SubCuencas.objects.all().order_by('id')

	listpolycuenca = []
	listpolycuenca.append([[[[-55.7609150966328, -27.3625], [-55.7609766656493, -27.3623152929484], [-55.7609350090908, -27.3613814917634], [-55.7605115007467, -27.3601179094906], [-55.7604629014285, -27.3600484818932], [-55.7614348877921, -27.3592570072828], [-55.7620701503084, -27.3585766168283], [-55.7642640623863, -27.3561605364387], [-55.7662080351135, -27.3550913514387], [-55.7679680247077, -27.3548344693283], [-55.7689677821103, -27.3553065769906], [-55.7700508526298, -27.3569589538088], [-55.770953411396, -27.3578059704971], [-55.7724391619804, -27.3580420243283], [-55.7739804546427, -27.3576948863413], [-55.7761882522401, -27.3562369067958], [-55.7782155380843, -27.3558481122504], [-55.7802983660064, -27.3560425095231], [-55.7819368573051, -27.3570978090036], [-55.7838252879544, -27.3576810008218], [-55.7865468497726, -27.3579864822504], [-55.7897127482141, -27.3580420243283], [-55.7937395488635, -27.3577087718608], [-55.7959334609414, -27.3583336202374], [-55.7992243290582, -27.3603053640036], [-55.8006961941232, -27.3605483605945], [-55.8029317627595, -27.3601734515686], [-55.8047924223699, -27.3590764955296], [-55.8097912093829, -27.3542721057894], [-55.8116171551946, -27.3516060860491], [-55.8127835388309, -27.3504674734517], [-55.8135611279218, -27.3500161940686], [-55.8159216662335, -27.3486068138413], [-55.8167825684413, -27.3490719787439], [-55.8181086355517, -27.3493982884517], [-55.8198582110062, -27.3491900056595], [-55.822107665162, -27.3485512717634], [-55.8237739274997, -27.3480791641011], [-55.825717900227, -27.3477181405946], [-55.8273008494477, -27.3471349487764], [-55.8296336167205, -27.344607784231], [-55.8309249700321, -27.3428443232569], [-55.8318830708763, -27.3420875624453], [-55.8328897710386, -27.3415182561466], [-55.8351669962334, -27.3412197174777], [-55.8393048810386, -27.3408309229323], [-55.8438315603892, -27.339636768257], [-55.8463031828567, -27.3386370108544], [-55.8474487382139, -27.3373526003024], [-55.8477542196424, -27.33554748277], [-55.8474209671749, -27.3334924258868], [-55.847337654058, -27.3301043591336], [-55.8492538557463, -27.325549908744], [-55.849503795097, -27.3243002119908], [-55.849503795097, -27.3238281043284], [-55.8548358345775, -27.3244390671856], [-55.8565020969152, -27.3250222590037], [-55.8581128171749, -27.3258553901726], [-55.8597790795126, -27.3262997267959], [-55.8607788369152, -27.3264385819908], [-55.860889921071, -27.3271328579648], [-55.8625, -27.3284209211056], [-55.8625, -27.3166666666644], [-55.8625, -27.3125], [-55.8541666666637, -27.3125], [-55.8541666666637, -27.308333333331], [-55.85, -27.308333333331], [-55.85, -27.3041666666644], [-55.8416666666637, -27.3041666666644], [-55.8416666666637, -27.3], [-55.8375, -27.3], [-55.8375, -27.2958333333311], [-55.8291666666637, -27.2958333333311], [-55.8291666666637, -27.3], [-55.8166666666637, -27.3], [-55.8166666666637, -27.2916666666644], [-55.8083333333304, -27.2916666666644], [-55.8083333333304, -27.2875], [-55.8041666666637, -27.2875], [-55.8041666666637, -27.283333333331], [-55.8, -27.283333333331], [-55.8, -27.2791666666644], [-55.7916666666637, -27.2791666666644], [-55.7916666666637, -27.275], [-55.7875, -27.275], [-55.7875, -27.2666666666644], [-55.7791666666637, -27.2666666666644], [-55.7791666666637, -27.2625], [-55.775, -27.2625], [-55.775, -27.2583333333311], [-55.7708333333304, -27.258333333331], [-55.7708333333304, -27.2625], [-55.7625, -27.2625], [-55.7625, -27.258333333331], [-55.7583333333304, -27.258333333331], [-55.7583333333304, -27.25], [-55.7541666666637, -27.25], [-55.7541666666637, -27.2458333333311], [-55.75, -27.2458333333311], [-55.75, -27.2416666666644], [-55.7416666666637, -27.2416666666644], [-55.7416666666637, -27.2375], [-55.7375, -27.2375], [-55.7375, -27.2291666666644], [-55.7333333333304, -27.2291666666644], [-55.7333333333304, -27.225], [-55.7291666666637, -27.225], [-55.7291666666637, -27.220833333331], [-55.725, -27.220833333331], [-55.725, -27.2166666666643], [-55.7166666666637, -27.2166666666644], [-55.7166666666637, -27.2208333333311], [-55.7166666666637, -27.225], [-55.7125, -27.225], [-55.7125, -27.2291666666643], [-55.7083333333304, -27.2291666666644], [-55.7083333333304, -27.233333333331], [-55.7041666666637, -27.233333333331], [-55.7041666666637, -27.25], [-55.7, -27.25], [-55.7, -27.258333333331], [-55.6958333333303, -27.258333333331], [-55.6958333333303, -27.2625], [-55.7, -27.2625], [-55.7, -27.2666666666644], [-55.7125, -27.2666666666644], [-55.7125, -27.270833333331], [-55.7083333333304, -27.270833333331], [-55.7083333333304, -27.2875], [-55.7125, -27.2875], [-55.7125, -27.3041666666644], [-55.7166666666637, -27.3041666666644], [-55.7166666666637, -27.3083333333311], [-55.725, -27.308333333331], [-55.725, -27.3166666666644], [-55.7333333333304, -27.3166666666644], [-55.7333333333304, -27.325], [-55.7416666666637, -27.325], [-55.7416666666637, -27.3375], [-55.7458333333304, -27.3375], [-55.7458333333304, -27.345833333331], [-55.75, -27.345833333331], [-55.75, -27.3541666666644], [-55.7583333333304, -27.3541666666644], [-55.7583333333304, -27.3625], [-55.7609150966328, -27.3625]]]])	
	listpolycuenca.append([[[[-55.8625, -27.3166666666644], [-55.8666666666637, -27.3166666666644], [-55.8666666666637, -27.3125], [-55.8708333333304, -27.3125], [-55.8708333333304, -27.3041666666644], [-55.875, -27.3041666666644], [-55.875, -27.3], [-55.875, -27.2958333333311], [-55.8791666666637, -27.2958333333311], [-55.8791666666637, -27.283333333331], [-55.875, -27.283333333331], [-55.875, -27.2791666666644], [-55.8708333333304, -27.2791666666644], [-55.8708333333304, -27.283333333331], [-55.8541666666637, -27.2833333333311], [-55.8416666666637, -27.283333333331], [-55.8416666666637, -27.2875], [-55.8375, -27.2875], [-55.8375, -27.2958333333311], [-55.8375, -27.3], [-55.8416666666637, -27.3], [-55.8416666666637, -27.3041666666644], [-55.85, -27.3041666666644], [-55.85, -27.308333333331], [-55.8541666666637, -27.308333333331], [-55.8541666666637, -27.3125], [-55.8625, -27.3125], [-55.8625, -27.3166666666644]]]])
	listpolycuenca.append([[[[-55.8541666666637, -27.2833333333311], [-55.8708333333304, -27.283333333331], [-55.8708333333304, -27.2791666666644], [-55.875, -27.2791666666644], [-55.875, -27.270833333331], [-55.8791666666637, -27.270833333331], [-55.8791666666637, -27.25], [-55.8708333333304, -27.25], [-55.8708333333304, -27.2458333333311], [-55.8625, -27.2458333333311], [-55.8625, -27.2375], [-55.8583333333304, -27.2375], [-55.8583333333304, -27.225], [-55.8541666666637, -27.225], [-55.8541666666637, -27.2166666666644], [-55.85, -27.2166666666644], [-55.85, -27.2125], [-55.8291666666637, -27.2125], [-55.8291666666637, -27.2041666666644], [-55.825, -27.2041666666644], [-55.825, -27.2], [-55.8208333333304, -27.2], [-55.8208333333304, -27.195833333331], [-55.8166666666637, -27.195833333331], [-55.8166666666637, -27.1916666666644], [-55.8125, -27.1916666666643], [-55.8125, -27.1875], [-55.8083333333304, -27.1875], [-55.8083333333304, -27.183333333331], [-55.8041666666637, -27.183333333331], [-55.8041666666637, -27.175], [-55.7958333333304, -27.175], [-55.7958333333304, -27.1791666666644], [-55.7875, -27.1791666666644], [-55.7875, -27.1833333333311], [-55.7833333333304, -27.183333333331], [-55.7833333333304, -27.1875], [-55.7791666666637, -27.1875], [-55.7791666666637, -27.1916666666644], [-55.7833333333304, -27.1916666666644], [-55.7833333333304, -27.195833333331], [-55.7875, -27.195833333331], [-55.7875, -27.2041666666644], [-55.7916666666637, -27.2041666666644], [-55.7916666666637, -27.2125], [-55.7958333333304, -27.2125], [-55.7958333333304, -27.2166666666644], [-55.8041666666637, -27.2166666666643], [-55.8041666666637, -27.225], [-55.8083333333304, -27.225], [-55.8083333333304, -27.2375], [-55.8041666666637, -27.2375], [-55.8041666666637, -27.2458333333311], [-55.8083333333304, -27.2458333333311], [-55.8083333333304, -27.25], [-55.8208333333304, -27.25], [-55.8208333333304, -27.2541666666644], [-55.825, -27.2541666666644], [-55.825, -27.2625], [-55.8291666666637, -27.2625], [-55.8291666666637, -27.2666666666644], [-55.85, -27.2666666666644], [-55.85, -27.270833333331], [-55.8541666666637, -27.270833333331], [-55.8541666666637, -27.2833333333311]]]])
	listpolycuenca.append([[[[-55.8833333333304, -27.3541666666644], [-55.8833333333304, -27.35], [-55.8875, -27.35], [-55.8875, -27.345833333331], [-55.8916666666637, -27.345833333331], [-55.8916666666637, -27.3416666666644], [-55.8958333333304, -27.3416666666644], [-55.8958333333304, -27.325], [-55.8916666666637, -27.325], [-55.8916666666637, -27.3125], [-55.8875, -27.3125], [-55.8875, -27.3041666666644], [-55.8791666666637, -27.3041666666644], [-55.8791666666637, -27.3], [-55.875, -27.3], [-55.875, -27.3041666666644], [-55.8708333333304, -27.3041666666644], [-55.8708333333304, -27.3125], [-55.8666666666637, -27.3125], [-55.8666666666637, -27.3166666666644], [-55.8625, -27.3166666666644], [-55.8625, -27.3375], [-55.8625, -27.3416666666644], [-55.875, -27.3416666666644], [-55.875, -27.3541666666644], [-55.8833333333304, -27.3541666666644]]]])
	listpolycuenca.append([[[[-55.8375, -27.2958333333311], [-55.8375, -27.2875], [-55.8416666666637, -27.2875], [-55.8416666666637, -27.283333333331], [-55.8541666666637, -27.2833333333311], [-55.8541666666637, -27.270833333331], [-55.85, -27.270833333331], [-55.85, -27.2666666666644], [-55.8291666666637, -27.2666666666644], [-55.8291666666637, -27.2625], [-55.825, -27.2625], [-55.825, -27.2541666666644], [-55.8208333333304, -27.2541666666644], [-55.8208333333304, -27.25], [-55.8083333333304, -27.25], [-55.8083333333304, -27.2458333333311], [-55.8041666666637, -27.2458333333311], [-55.8041666666637, -27.2375], [-55.8083333333304, -27.2375], [-55.8083333333304, -27.225], [-55.8041666666637, -27.225], [-55.8041666666637, -27.2166666666643], [-55.7958333333304, -27.2166666666644], [-55.7958333333304, -27.2125], [-55.7916666666637, -27.2125], [-55.7916666666637, -27.2041666666644], [-55.7875, -27.2041666666644], [-55.7875, -27.195833333331], [-55.7833333333304, -27.195833333331], [-55.7833333333304, -27.1916666666644], [-55.7791666666637, -27.1916666666644], [-55.7458333333304, -27.1916666666643], [-55.7458333333304, -27.2041666666644], [-55.7416666666637, -27.2041666666644], [-55.7416666666637, -27.208333333331], [-55.7333333333304, -27.2083333333311], [-55.7333333333304, -27.2125], [-55.725, -27.2125], [-55.725, -27.2166666666643], [-55.725, -27.220833333331], [-55.7291666666637, -27.220833333331], [-55.7291666666637, -27.225], [-55.7333333333304, -27.225], [-55.7333333333304, -27.2291666666644], [-55.7375, -27.2291666666644], [-55.7375, -27.2375], [-55.7416666666637, -27.2375], [-55.7416666666637, -27.2416666666644], [-55.75, -27.2416666666644], [-55.75, -27.2458333333311], [-55.7541666666637, -27.2458333333311], [-55.7541666666637, -27.25], [-55.7583333333304, -27.25], [-55.7583333333304, -27.258333333331], [-55.7625, -27.258333333331], [-55.7625, -27.2625], [-55.7708333333304, -27.2625], [-55.7708333333304, -27.258333333331], [-55.775, -27.2583333333311], [-55.775, -27.2625], [-55.7791666666637, -27.2625], [-55.7791666666637, -27.2666666666644], [-55.7875, -27.2666666666644], [-55.7875, -27.275], [-55.7916666666637, -27.275], [-55.7916666666637, -27.2791666666644], [-55.8, -27.2791666666644], [-55.8, -27.283333333331], [-55.8041666666637, -27.283333333331], [-55.8041666666637, -27.2875], [-55.8083333333304, -27.2875], [-55.8083333333304, -27.2916666666644], [-55.8166666666637, -27.2916666666644], [-55.8166666666637, -27.3], [-55.8291666666637, -27.3], [-55.8291666666637, -27.2958333333311], [-55.8375, -27.2958333333311]]]])
	listpolycuenca.append([[[[-55.8625, -27.3284209211056], [-55.860889921071, -27.3271328579648], [-55.8607788369152, -27.3264385819908], [-55.8597790795126, -27.3262997267959], [-55.8581128171749, -27.3258553901726], [-55.8565020969152, -27.3250222590037], [-55.8548358345775, -27.3244390671856], [-55.849503795097, -27.3238281043284], [-55.849503795097, -27.3243002119908], [-55.8492538557463, -27.325549908744], [-55.847337654058, -27.3301043591336], [-55.8474209671749, -27.3334924258868], [-55.8477542196424, -27.33554748277], [-55.8474487382139, -27.3373526003024], [-55.8463031828567, -27.3386370108544], [-55.8438315603892, -27.339636768257], [-55.8393048810386, -27.3408309229323], [-55.8351669962334, -27.3412197174777], [-55.8328897710386, -27.3415182561466], [-55.8318830708763, -27.3420875624453], [-55.8309249700321, -27.3428443232569], [-55.8296336167205, -27.344607784231], [-55.8273008494477, -27.3471349487764], [-55.825717900227, -27.3477181405946], [-55.8237739274997, -27.3480791641011], [-55.822107665162, -27.3485512717634], [-55.8198582110062, -27.3491900056595], [-55.8181086355517, -27.3493982884517], [-55.8167825684413, -27.3490719787439], [-55.8159216662335, -27.3486068138413], [-55.8135611279218, -27.3500161940686], [-55.8127835388309, -27.3504674734517], [-55.8116171551946, -27.3516060860491], [-55.8097912093829, -27.3542721057894], [-55.8047924223699, -27.3590764955296], [-55.8029317627595, -27.3601734515686], [-55.8006961941232, -27.3605483605945], [-55.7992243290582, -27.3603053640036], [-55.7959334609414, -27.3583336202374], [-55.7937395488635, -27.3577087718608], [-55.7897127482141, -27.3580420243283], [-55.7865468497726, -27.3579864822504], [-55.7838252879544, -27.3576810008218], [-55.7819368573051, -27.3570978090036], [-55.7802983660064, -27.3560425095231], [-55.7782155380843, -27.3558481122504], [-55.7761882522401, -27.3562369067958], [-55.7739804546427, -27.3576948863413], [-55.7724391619804, -27.3580420243283], [-55.770953411396, -27.3578059704971], [-55.7700508526298, -27.3569589538088], [-55.7689677821103, -27.3553065769906], [-55.7679680247077, -27.3548344693283], [-55.7662080351135, -27.3550913514387], [-55.7642640623863, -27.3561605364387], [-55.7620701503084, -27.3585766168283], [-55.7614348877921, -27.3592570072828], [-55.7604629014285, -27.3600484818932], [-55.7605115007467, -27.3601179094906], [-55.7609350090908, -27.3613814917634], [-55.7609766656493, -27.3623152929484], [-55.7609150966328, -27.3625], [-55.7625, -27.3625], [-55.7625, -27.375], [-55.7666666666637, -27.375], [-55.7666666666637, -27.3791666666644], [-55.775, -27.3791666666644], [-55.775, -27.3875], [-55.8041666666637, -27.3875], [-55.8041666666637, -27.383333333331], [-55.8125, -27.383333333331], [-55.8125, -27.3791666666644], [-55.8208333333304, -27.3791666666644], [-55.8208333333304, -27.375], [-55.825, -27.375], [-55.825, -27.370833333331], [-55.8291666666637, -27.370833333331], [-55.8375, -27.370833333331], [-55.8375, -27.3666666666644], [-55.8458333333304, -27.3666666666644], [-55.8458333333304, -27.3625], [-55.8541666666637, -27.3625], [-55.8541666666637, -27.3541666666644], [-55.8583333333304, -27.3541666666644], [-55.8583333333304, -27.3375], [-55.8625, -27.3375], [-55.8625, -27.3284209211056]]]])


	list_comid = subcuencas.values_list('comid', flat=True)
	list_grid_code = subcuencas.values_list('grid_code', flat=True)
	list_grid_count = subcuencas.values_list('grid_count', flat=True)
	list_areasqkm = subcuencas.values_list('areasqkm', flat=True)

	vol_en_cuenca = DataHistSubc.objects.order_by('-fecha_subc','id_subc')[:6]
	acu_subc = DataVolAcumSubcuencas.objects.order_by('-fecha_subc','id_subc')[:2]

	list_fecha_subc = vol_en_cuenca.values_list('fecha_subc', flat=True)
	list_vol_subc = list(vol_en_cuenca.values_list('vol_subc', flat=True))

	for key, valor in enumerate(list_vol_subc):
		if key == 4:
			list_vol_subc[4] = (acu_subc[0].vol_acum/1000000000)
		elif key == 5:
			list_vol_subc[5] = (acu_subc[1].vol_acum/1000000000)
		else:
			list_vol_subc[key] = 0

	list_id = ["1","2","3","4","5","6"]

	serializado = serialize('geojson', subcuencas, indent=2)
	geo_json = [
				{
				  "type": "Feature",
				  "properties": {
					"comid": comid,
					"grid_code": grid_code,
					"grid_count": grid_count,
					"prod_unit": "3",
					"areasqkm": areasqkm,
					"pk": pk,	
					"fecha_observ": fecha_subc,
					"vol_en_cuenca": vol_subc
				  },
			  	"geometry": {
					"type": "MultiPolygon",
					"coordinates": poly

					}
				}
			for pk, comid, grid_code, grid_count, areasqkm, fecha_subc, vol_subc, poly in zip(list_id, list_comid, list_grid_code, list_grid_count, list_areasqkm, list_fecha_subc, list_vol_subc, listpolycuenca)]
	
	georesp = JsonResponse({"type": "FeatureCollection", "crs": {"type": "name", "properties": {"name": "EPSG:4326"}}, "features": geo_json}, safe=False)
	return HttpResponse(georesp, content_type='geojson')

def precipitacion(request):
	return render(
		request,
		'precipitacion.html',
	)
#ver para consumir datos para el grafico
fecha_hasta = '2017-11-07 00:00:00'
def get_rain_range(cuenca, fecha_inicio):
	#rango_lluvia = DataHistSubc.objects.filter(fecha_subc__range = (fecha_inicio, fecha_hasta), 
	#  id_subc=cuenca).order_by('fecha_subc')
	#variant_vol = DataVolAcumSubcuencas.objects.filter(fecha_subc__range = (fecha_inicio, fecha_hasta), 
	#  id_subc = cuenca).order_by('fecha_subc')
	#rango_lluvia = DataHistSubc.objects.filter(id_subc=cuenca).order_by('-fecha_subc')[:12]
	#variant_vol = DataVolAcumSubcuencas.objects.filter(id_subc = cuenca).order_by('-fecha_subc')[:12]
	#-rango_lluvia = DataHistSubc.objects.filter(id_subc=cuenca, fecha_subc__range =(fecha_inicio, fecha_hasta)).order_by('-fecha_subc')[:12]
	#rango_lluvia = rango_lluvia.order_by('fecha_subc')
	#-variant_vol = DataVolAcumSubcuencas.objects.filter(id_subc = cuenca,  fecha_subc__range =(fecha_inicio, fecha_hasta)).order_by('-fecha_subc')[:12]
	#variant_vol = variant_vol.order_by('fecha_subc')
	
	#json = serialize('json', rango_lluvia, indent=2)
	#-list_fechas = rango_lluvia.values_list('fecha_subc', flat=True)
	#-list_valores = rango_lluvia.values_list('vol_subc', flat=True)
	#-lista_acumulada = variant_vol.values_list('vol_acum', flat=True)

	#-------------------------------------------------------------------------------------------------------
	#-----Obtener los ultimos registros de lluvia descargados GPM-------
	#-----obtiene los datos de los pixeles de las subcuencas 5 y 6------
	segmentos = SegCuencas.objects.filter(id_subc=cuenca).order_by('id_pix')

	#realizar la cuantificación de la lluvia por area afectada en el por momento X

	pronostlluviaensubc = [] #para lista de lluvia en subcuenca
	pronostaguaensubc = [] #para acumulador de volumen en subcuenca
	pronostlluviacaida = [] #para lista de lluvia caida en subcuenca en milímetros
	lluviaensubcreal = []
	aguaensubcreal = []
	lluviacaidareal = []
	
	if (cuenca == 5):
		#-----para la subcuenca 5
		last_date_precip = PrecipData.objects.filter(id_pix__in=[10,15,16], fecha=fecha_inicio).order_by('-fecha')[:3]
		print("last id: ", last_date_precip[0].id_pd)
		#-----Obtener los datos de las predicciones de los registros (se deben obtener por pixel) -------
		predicpix10 = PronostPrecip.objects.filter(id_pd=last_date_precip[0].id_pd).order_by('fecha_pprecip')
		predicpix15 = PronostPrecip.objects.filter(id_pd=last_date_precip[1].id_pd).order_by('fecha_pprecip')
		predicpix16 = PronostPrecip.objects.filter(id_pd=last_date_precip[2].id_pd).order_by('fecha_pprecip')

		#----Obtener datos reales, solicitado y 4 previos-----------------
		pixel10pre = PrecipData.objects.filter(id_pix=10, fecha__lte=fecha_inicio).order_by('-fecha')[:5]
		pixel15pre = PrecipData.objects.filter(id_pix=15, fecha__lte=fecha_inicio).order_by('-fecha')[:5]
		pixel16pre = PrecipData.objects.filter(id_pix=16, fecha__lte=fecha_inicio).order_by('-fecha')[:5]

		#----Obtener datos reales, solicitado y 4 previos-----------------
		pixel10pos = PrecipData.objects.filter(id_pix=10, fecha__gt=fecha_inicio).order_by('fecha')[:len(predicpix10)-5]
		pixel15pos = PrecipData.objects.filter(id_pix=15, fecha__gt=fecha_inicio).order_by('fecha')[:len(predicpix15)-5]
		pixel16pos = PrecipData.objects.filter(id_pix=16, fecha__gt=fecha_inicio).order_by('fecha')[:len(predicpix16)-5]
		mmpix10 = []
		mmpix15 = []
		mmpix16 = []


		for i in reversed(range(len(pixel10pre))):
			mmpix10.append(float(pixel10pre[i].mm_h))
			mmpix15.append(float(pixel15pre[i].mm_h))
			mmpix16.append(float(pixel16pre[i].mm_h))

		for i in range(len(pixel10pos)):
			mmpix10.append(float(pixel10pos[i].mm_h))
			mmpix15.append(float(pixel15pos[i].mm_h))
			mmpix16.append(float(pixel16pos[i].mm_h))
		
		list_fechas = predicpix10.values_list('fecha_pprecip', flat=True) # lista de fechas
		for i in range(len(list_fechas)):
			#-----Cuatifica la lluvia por segmento------------------------------
			pronostlluviaenseg13 = float(predicpix10[i].valor_pprecip) * (segmentos[0].area_cobxpix * 1000000000000)
			pronostlluviaenseg14 = float(predicpix15[i].valor_pprecip) * (segmentos[1].area_cobxpix * 1000000000000)
			pronostlluviaenseg15 = float(predicpix16[i].valor_pprecip) * (segmentos[2].area_cobxpix * 1000000000000)
			#-----Suma las lluvias para la subcuenca----------------------------
			sumalluvia = float(pronostlluviaenseg13) + float(pronostlluviaenseg14) + float(pronostlluviaenseg15)
			pronostlluviaensubc.append(round(((sumalluvia*0.6)/1000000000),2))

			#-----Agrega a la lista de lluvia caída
			sumamilim = float(predicpix10[i].valor_pprecip)*0.0556 + float(predicpix15[i].valor_pprecip)*0.3245 + float(predicpix16[i].valor_pprecip)*0.6199
			pronostlluviacaida.append(round((sumamilim),2))

			# contador, empezar a acumular desde el ultimo volumen real registrado
			# si predicpix10[0].fecha_pprecip es menor o igual a last_date_precip[0].fecha
			if (predicpix10[i].fecha_pprecip <= last_date_precip[0].fecha):
				#rescatar valor desde la base de datos
				volumen = DataVolAcumSubcuencas.objects.filter(fecha_subc = predicpix10[i].fecha_pprecip, id_subc=5)
				aguaensubcreal.append(round(((volumen[0].vol_acum*0.6)/1000000000),2))
				if (i < 4):
					pronostaguaensubc.append('null')
				else:
					pronostaguaensubc.append(round(((volumen[0].vol_acum*0.6)/1000000000),2))

				lluviaenseg13 = float(mmpix10[i]) * (segmentos[0].area_cobxpix * 1000000000000)
				lluviaenseg14 = float(mmpix15[i]) * (segmentos[1].area_cobxpix * 1000000000000)
				lluviaenseg15 = float(mmpix16[i]) * (segmentos[2].area_cobxpix * 1000000000000)
				
				sumalluvia = float(lluviaenseg13) + float(lluviaenseg14) + float(lluviaenseg15)
				lluviaensubcreal.append(round(((sumalluvia*0.6)/1000000000),2))

				sumamilim = float(mmpix10[i])*0.0556 + float(mmpix15[i])*0.3245 + float(mmpix16[i])*0.6199
				lluviacaidareal.append(round((sumamilim),2))
			else:
				# continuar acumulado con datos predichos
				# para subcuenca 5 Bº Sarita se divide con 16 para descargar la subcuenca
				cargasubc = float(pronostaguaensubc[i-1]) + float(pronostlluviaensubc[i]) - float(float(pronostaguaensubc[i-1])/16)
				pronostaguaensubc.append(round((cargasubc),2))
				volumen = DataVolAcumSubcuencas.objects.filter(fecha_subc = predicpix10[i].fecha_pprecip, id_subc=5)
				aguaensubcreal.append(round(((volumen[0].vol_acum*0.6)/1000000000),2))
				#aguaensubcreal.append('null')
				lluviaensubcreal.append('null')
				sumamilim = float(mmpix10[i])*0.0556 + float(mmpix15[i])*0.3245 + float(mmpix16[i])*0.6199
				lluviacaidareal.append(round((sumamilim),2))
				#lluviacaidareal.append('null')
	else:
		#-----para la subcuenca 6
		last_date_precip = PrecipData.objects.filter(id_pix__in=[21,22], fecha=fecha_inicio).order_by('-fecha')[:2]
		#-----Obtener los datos de las predicciones de los registros (se deben obtener por pixel) -------
		predicpix21 = PronostPrecip.objects.filter(id_pd=last_date_precip[0].id_pd).order_by('fecha_pprecip')
		predicpix22 = PronostPrecip.objects.filter(id_pd=last_date_precip[1].id_pd).order_by('fecha_pprecip')

		#----Obtener datos reales, solicitado y 4 previos-----------------
		pixel21pre = PrecipData.objects.filter(id_pix=21, fecha__lte=fecha_inicio).order_by('-fecha')[:5]
		pixel22pre = PrecipData.objects.filter(id_pix=22, fecha__lte=fecha_inicio).order_by('-fecha')[:5]

		#----Obtener datos reales, solicitado y 4 previos-----------------
		pixel21pos = PrecipData.objects.filter(id_pix=21, fecha__gt=fecha_inicio).order_by('fecha')[:len(predicpix21)-5]
		pixel22pos = PrecipData.objects.filter(id_pix=22, fecha__gt=fecha_inicio).order_by('fecha')[:len(predicpix22)-5]

		mmpix21 = []
		mmpix22 = []

		for i in reversed(range(len(pixel21pre))):
			mmpix21.append(float(pixel21pre[i].mm_h))
			mmpix22.append(float(pixel22pre[i].mm_h))

		for i in range(len(pixel21pos)):
			mmpix21.append(float(pixel21pos[i].mm_h))
			mmpix22.append(float(pixel22pos[i].mm_h))

		list_fechas = predicpix21.values_list('fecha_pprecip', flat=True) # lista de fechas

		for i in range(len(list_fechas)):
			#-----Cuatifica la lluvia por segmento------------------------------
			pronostlluviaenseg16 = float(predicpix21[i].valor_pprecip) * (segmentos[0].area_cobxpix * 1000000000000)
			pronostlluviaenseg17 = float(predicpix22[i].valor_pprecip) * (segmentos[1].area_cobxpix * 1000000000000)

			#-----Suma las lluvias para la subcuenca----------------------------
			sumalluvia = float(pronostlluviaenseg16) + float(pronostlluviaenseg17)
			pronostlluviaensubc.append(round(((sumalluvia*0.6)/1000000000),2))

			sumamilim = float(predicpix21[i].valor_pprecip)*0.6288 + float(predicpix22[i].valor_pprecip)*0.3712

			pronostlluviacaida.append(round((sumamilim),2))
			# contador, empezar a acumular desde el ultimo volumen real registrado
			#si predicpix10[0].fecha_pprecip es menor o igual a last_date_precip[0].fecha
			if (predicpix21[i].fecha_pprecip <= last_date_precip[0].fecha):
				#rescatar valor desde la base de datos
				volumen = DataVolAcumSubcuencas.objects.filter(fecha_subc = predicpix21[i].fecha_pprecip, id_subc=6)
				aguaensubcreal.append(round(((volumen[0].vol_acum*0.6)/1000000000),2))
				if (i < 4):
					pronostaguaensubc.append('null')
				else:
					pronostaguaensubc.append(round(((volumen[0].vol_acum*0.6)/1000000000),2))

				lluviaenseg16 = float(mmpix21[i]) * (segmentos[0].area_cobxpix * 1000000000000)
				lluviaenseg17 = float(mmpix22[i]) * (segmentos[1].area_cobxpix * 1000000000000)
				
				sumalluvia = float(lluviaenseg16) + float(lluviaenseg17)
				lluviaensubcreal.append(round(((sumalluvia*0.6)/1000000000),2))

				sumamilim = float(mmpix21[i])*0.6288 + float(mmpix22[i])*0.3712
				lluviacaidareal.append(round((sumamilim),2))
			else:
				# continuar acumulado con datos predichos
				#para subcuenca 6 Bº Fátima se divide con 8 para descargar la subcuenca
				cargasubc = float(pronostaguaensubc[i-1]) + float(pronostlluviaensubc[i]) - float(float(pronostaguaensubc[i-1])/8)
				pronostaguaensubc.append(round((cargasubc),2))
				volumen = DataVolAcumSubcuencas.objects.filter(fecha_subc = predicpix21[i].fecha_pprecip, id_subc=6)
				aguaensubcreal.append(round(((volumen[0].vol_acum*0.6)/1000000000),2))
				#aguaensubcreal.append('null')
				lluviaensubcreal.append('null')
				sumamilim = float(mmpix21[i])*0.6288 + float(mmpix22[i])*0.3712
				lluviacaidareal.append(round((sumamilim),2))
				#lluviacaidareal.append('null')
		
	#convierte de UTC a hora local
	time_list = []
	local_tz = timezone('America/Asuncion')

	for x in list_fechas:
		hora = x.astimezone(local_tz)
		time_list.append(hora.strftime("%H:%M"))
	print(time_list)
	
	fechaobs = last_date_precip[0].fecha
	fechaobs = fechaobs.strftime(("%Y-%m-%d %H:%M"))
	for item, fecha, vol, volluvia in zip(pronostlluviacaida, time_list, pronostaguaensubc, pronostlluviaensubc):
		print(fecha, " lluvia", item, "milímetros", "/ acumulado: ", vol, "/ momento: ", volluvia)
	#-------------------------------------------------------------------------------------------------------
	#genera el JSON
	json=[{
			#'fecha': fechas.strftime("%Y-%m-%d %H:%M"),
			'fecha': fechas,#fechas.strftime("%H:%M"), #ver para pasar a horario py -4 UTC
			#'lluvia':round((valores/1000000000),2), #Volumen de lluvia en el momento sacar de pronostlluviaensubc
			'lluvia':valores, #Volumen de lluvia en el momento
			'lluviareal': lluviareal,
			'acumulado': acum,
			'acumreal': acumreal
			}
			#for valores, fechas, acum in zip(list_valores, time_list, lista_acumulada)]
			for valores, fechas, acum, lluviareal, acumreal in zip(pronostlluviacaida, time_list, pronostaguaensubc, lluviacaidareal, aguaensubcreal)]
	return json, fechaobs

def saritapronostic(request,):
	# if this is a POST request we need to process the form data
	if request.method == 'POST':
		# create a form instance and populate it with data from the request:
		form = XDSoftEventForm(request.POST)
		# check whether it's valid:
		if form.is_valid():
			# process the data in form.cleaned_data as required
			# ...
			newdate = form.cleaned_data['date']
			existfecha = PronostPrecip.objects.filter(id_pd__fecha=newdate)[:1]
			
			if existfecha.count() > 0:
				print(newdate)
				lluvia_cuenca2, fecha = get_rain_range(5, newdate)

				# redirect to a new URL:
				return render(
					request,
					'saritapronsotic.html',
					{'dataset1': lluvia_cuenca2, 'fecha': fecha, 'form': form, 'bandera': 1}
				)
			else:
				return render(
					request,
					'saritapronsotic.html',
					{'fecha_solicitada': newdate.strftime(("%d-%m-%Y %H:%M")), 'bandera': 0, 'form': form})
		else:
			return render(
				request,
				'saritapronsotic.html',
				{'bandera': 0, 'form': form})
				
	# if a GET (or any other method) we'll create a blank form
	else:
		form = XDSoftEventForm
		last_predic = PronostPrecip.objects.order_by('-fecha_pprecip')[:1]
		fecha_inicio = last_predic[0].id_pd.fecha
		#fecha_inicio = '2017-11-07 00:00:00'
		lluvia_cuenca2, fecha = get_rain_range(5, fecha_inicio)
		
		return render(
			request,
			'saritapronsotic.html',
			{'dataset1': lluvia_cuenca2, 'fecha': fecha, 'form': form, 'bandera': 1}
		)

def fatimapronostic(request,):
	# if this is a POST request we need to process the form data
	if request.method == 'POST':
		# create a form instance and populate it with data from the request:
		form = XDSoftEventForm(request.POST)
		# check whether it's valid:
		if form.is_valid():
			# process the data in form.cleaned_data as required
			# ...
			newdate = form.cleaned_data['date']
			existfecha = PronostPrecip.objects.filter(id_pd__fecha=newdate)[:1]
			
			if existfecha.count() > 0:
				print(newdate)
				lluvia_cuenca3, fecha = get_rain_range(6, newdate)

				# redirect to a new URL:
				return render(
					request,
					'fatimapronsotic.html',
					{'dataset2': lluvia_cuenca3, 'fecha': fecha, 'form': form, 'bandera': 1}
				)
					#return HttpResponseRedirect('/vercapas/graficos/')
			else:
				return render(
					request,
					'fatimapronsotic.html',
					{'fecha_solicitada': newdate.strftime(("%d-%m-%Y %H:%M")), 'bandera': 0, 'form': form})
		else:
			return render(

				request,
				'fatimapronsotic.html',
				{'bandera': 0, 'form': form})
		
	# if a GET (or any other method) we'll create a blank form
	else:
		form = XDSoftEventForm
		last_predic = PronostPrecip.objects.order_by('-fecha_pprecip')[:1]
		fecha_inicio = last_predic[0].id_pd.fecha
		#fecha_inicio = '2017-11-07 00:00:00'
		lluvia_cuenca3, fecha = get_rain_range(6, fecha_inicio)
		
		return render(
			request,
			'fatimapronsotic.html',
			{'dataset2': lluvia_cuenca3, 'fecha': fecha, 'form': form, 'bandera': 1}
		)

def realnpronostico (cuenca, fecha_solicitada):

	fecha_inicio = fecha_solicitada - timedelta(hours=2)
	valoresreales = []
	valorespronosticados = []
	print("estás en realnpronostico")
	if cuenca == 5:
		registros = 144
		#registros = 288
		print("estás en Cuenca 5")
		observado = PrecipData.objects.filter(id_pix__in=[10,15,16], fecha__gte = fecha_inicio).order_by('fecha', 'id_pix')[:registros]
		segmentos = SegCuencas.objects.filter(id_subc=5)
		listafechas = observado.values_list('fecha', flat=True)
		mmpix10 = []
		mmpix15 = []
		mmpix16 = []
		pronpix10 = []
		pronpix15 = []
		pronpix16 = []
		rangofechas = []
		acumulado =[]
		pronostacum =[]

		i = 0

		while i < registros:
		#for i in range(144):
			print("el momento procesado de la Subcuenca 5 es:", listafechas[i])
			pronostico = PronostPrecip.objects.filter(fecha_pprecip=listafechas[i], id_pd=observado[i].id_pd)[:1]
			if(len(pronostico) > 0):
				if (pronostico[0].id_pd.id_pix.id_pix == 10):
					pronpix10.append(float(pronostico[0].valor_pprecip))
					mmpix10.append(float(observado[i].mm_h))
					rangofechas.append(listafechas[i])
				if (pronostico[0].id_pd.id_pix.id_pix == 15):
					pronpix15.append(float(pronostico[0].valor_pprecip))
					mmpix15.append(float(observado[i].mm_h))
				if (pronostico[0].id_pd.id_pix.id_pix == 16):
					pronpix16.append(float(pronostico[0].valor_pprecip))
					mmpix16.append(float(observado[i].mm_h))
				i += 1
			else:
				mmpix10.append(float(observado[i].mm_h))
				mmpix15.append(float(observado[i+1].mm_h))
				mmpix16.append(float(observado[i+2].mm_h))
				pronpix10.append('null')
				pronpix15.append('null')
				pronpix16.append('null')
				rangofechas.append(listafechas[i])
				i += 3


		for i in range(len(mmpix10)):
			volumen = DataVolAcumSubcuencas.objects.filter(fecha_subc=rangofechas[i],id_subc=5)
			acumulado.append(round(float(volumen[0].vol_acum*0.6),2))
			if(mmpix10[i] != 'null'):
				sumamilim = float(mmpix10[i])*0.0556 + float(mmpix15[i])*0.3245 + float(mmpix16[i])*0.6199
				valoresreales.append(round((sumamilim),2))
			else:
				valoresreales.append('null')

			if(pronpix10[i] != 'null'):
				sumamilim = float(pronpix10[i])*0.0556 + float(pronpix15[i])*0.3245 + float(pronpix16[i])*0.6199
				valorespronosticados.append(round((sumamilim),2))
				subcuenca = float(segmentos[0].area_cobxpix) + float(segmentos[1].area_cobxpix) + float(segmentos[2].area_cobxpix)
				aguaensubpronos = (float(sumamilim/1000) * float(subcuenca/0.0000010000))*0.6
				if(i >= 1):
					aguaensubpronos = (float(pronostacum[i-1]) + float(aguaensubpronos) - float(float(pronostacum[i-1])/16))
					pronostacum.append(round((aguaensubpronos),2))
				else:
					pronostacum.append(round(float(volumen[0].vol_acum*0.6),2))
			else:
				valorespronosticados.append('null')
				pronostacum.append('null')


	else:
		print("estás en Cuenca 6")
		registros = 96
		#registros = 192
		observado = PrecipData.objects.filter(id_pix__in=[21,22], fecha__gte = fecha_inicio).order_by('fecha', 'id_pix')[:registros]
		segmentos = SegCuencas.objects.filter(id_subc=6).order_by('id_pix')
		listafechas = observado.values_list('fecha', flat=True)
		mmpix21 = []
		mmpix22 = []
		pronpix21 = []
		pronpix22 = []
		rangofechas = []
		acumulado =[]
		pronostacum =[]
		i = 0
		while i < registros:
		#for i in range(96):
			print("el momento procesado de la Subcuenca 6 es:", listafechas[i])
			pronostico = PronostPrecip.objects.filter(fecha_pprecip=listafechas[i], id_pd=observado[i].id_pd)[:1]
			if(len(pronostico) > 0):
				if (pronostico[0].id_pd.id_pix.id_pix == 21):
					pronpix21.append(float(pronostico[0].valor_pprecip))
					mmpix21.append(float(observado[i].mm_h))
					rangofechas.append(listafechas[i])
				if (pronostico[0].id_pd.id_pix.id_pix == 22):
					pronpix22.append(float(pronostico[0].valor_pprecip))
					mmpix22.append(float(observado[i].mm_h))
				i += 1
			else:
				mmpix21.append(float(observado[i].mm_h))
				mmpix22.append(float(observado[i+1].mm_h))
				pronpix21.append('null')
				pronpix22.append('null')
				rangofechas.append(listafechas[i])
				i += 2
#
		for i in range(len(mmpix21)):
			volumen = DataVolAcumSubcuencas.objects.filter(fecha_subc=rangofechas[i],id_subc=6)
			acumulado.append(round(float(volumen[0].vol_acum*0.6),2))
			if(mmpix21[i] != 'null'):
				sumamilim = float(mmpix21[i])*0.6288 + float(mmpix22[i])*0.3712
				valoresreales.append(round((sumamilim),2))
			else:
				valoresreales.append('null')
			if(pronpix21[i] != 'null'):
				sumamilim = float(pronpix21[i])*0.6288 + float(pronpix22[i])*0.3712
				valorespronosticados.append(round((sumamilim),2))
				subcuenca = float(segmentos[0].area_cobxpix) + float(segmentos[1].area_cobxpix)
				aguaensubcreal = (float(sumamilim/1000) * float(subcuenca/0.0000010000))*0.6
				if(i >= 1):
					aguaensubcreal = float(pronostacum[i-1]) + float(aguaensubcreal) - float(float(pronostacum[i-1])/8)
					pronostacum.append(round((aguaensubcreal),2))
				else:
					pronostacum.append(round(float(volumen[0].vol_acum*0.6),2))
			else:
				valorespronosticados.append('null')

	finicio = fecha_inicio.strftime("%H:%M %d/%m/%Y")
	fcierre = listafechas[len(listafechas)-1].strftime("%H:%M %d/%m/%Y")

	#fechaobs = fechaobs.strftime(("%Y-%m-%d %H:%M"))
	#for item, fecha, vol, volluvia in zip(pronostlluviacaida, time_list, pronostaguaensubc, pronostlluviaensubc):
	#	print(fecha, " lluvia", item, "milímetros", "/ acumulado: ", vol, "/ momento: ", volluvia)
#-------------------------------------------------------------------------------------------------------
	#genera el JSON
	json=[{
			'horas': horas.strftime("%H:%M"),#fechas.strftime("%H:%M"), #ver para pasar a horario py -4 UTC
			'lluviareal':real, #Volumen de lluvia en el momento
			'lluviapron': pronostico,
			'volacumulado': acum,
			'pronostacum':acumpredic
			}
			#for valores, fechas, acum in zip(list_valores, time_list, lista_acumulada)]
			for horas, real, pronostico, acum, acumpredic in zip(rangofechas, valoresreales, valorespronosticados, acumulado, pronostacum)]
	return json, finicio, fcierre
fecha_compare= "2018-09-13 02:00:00"
def saritacompare(request,):
	# if this is a POST request we need to process the form data
	if request.method == 'POST':
		# create a form instance and populate it with data from the request:
		form = XDSoftEventForm(request.POST)
		# check whether it's valid:
		if form.is_valid():
			# process the data in form.cleaned_data as required
			newdate = form.cleaned_data['date']
			existfecha = PrecipData.objects.filter(fecha=newdate)[:1]

			if existfecha.count() > 0:
				print(newdate)
				lluvia_cuenca, finicio, fcierre = realnpronostico(5, newdate)

				# redirect to a new URL:
				return render(
					request,
					'saritacompare.html',
					{'dataset1': lluvia_cuenca, 'fecha_inicio': finicio, 'fecha_cierre': fcierre, 'form': form}
				)
			else:
				print("Todavía no existen registros de esa fecha")
				return HttpResponseRedirect('/vercapas/saritacompare/')

	# if a GET (or any other method) we'll create a blank form
	else:
		form = XDSoftEventForm
		fecha_inicio = datetime.strptime(fecha_compare, "%Y-%m-%d %H:%M:%S") #2019-07-24 00:00:00
		lluvia_cuenca, finicio, fcierre = realnpronostico(5, fecha_inicio)
		
		return render(
			request,
			'saritacompare.html',
			{'dataset1': lluvia_cuenca, 'fecha_inicio': finicio, 'fecha_cierre': fcierre, 'form': form}
		)

def fatimacompare(request,):
	# if this is a POST request we need to process the form data
	if request.method == 'POST':
		# create a form instance and populate it with data from the request:
		form = XDSoftEventForm(request.POST)
		# check whether it's valid:
		if form.is_valid():
			# process the data in form.cleaned_data as required
			newdate = form.cleaned_data['date']
			existfecha = PrecipData.objects.filter(fecha=newdate)[:1]

			if existfecha.count() > 0:
				print(newdate)
				lluvia_cuenca, finicio, fcierre = realnpronostico(6, newdate)

				# redirect to a new URL:
				return render(
					request,
					'fatimacompare.html',
					{'dataset2': lluvia_cuenca, 'fecha_inicio': finicio, 'fecha_cierre': fcierre, 'form': form}
				)
			else:
				print("Todavía no existen registros de esa fecha")
				return HttpResponseRedirect('/vercapas/fatimacompare/')

	# if a GET (or any other method) we'll create a blank form
	else:
		form = XDSoftEventForm
		fecha_inicio = datetime.strptime(fecha_compare, "%Y-%m-%d %H:%M:%S") #2019-07-24 00:00:00
		lluvia_cuenca, finicio, fcierre = realnpronostico(6, fecha_inicio)
		
		return render(
			request,
			'fatimacompare.html',
			{'dataset2': lluvia_cuenca, 'fecha_inicio': finicio, 'fecha_cierre': fcierre, 'form': form}
		)

def get_date(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = XDSoftEventForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            return HttpResponseRedirect('/vercapas/buscador/')

    # if a GET (or any other method) we'll create a blank form
    else:
        form = XDSoftEventForm

    form.helper.form_class = 'form-inline'

    return render(request, 'datespicker.html', {'form': form})

def XDSoftDateTimePickerView(request):
    form_class = XDSoftEventForm
    return render(
        request,
        'xdsoft_event_form.html', {'form': form_class}
        )

def equipo(request):
	
	return render(
		request,
		'team.html',
	)

def proyecto(request):
	
	return render(
		request,
		'project.html',
	)

def faq(request):
	return render(
		request,
		'faq.html',
	)