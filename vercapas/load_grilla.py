import os
from django.contrib.gis.utils import LayerMapping
from .models import Grilla

grilla_mapping = {
    'id' : 'id',
    'x_min' : 'xmin_1',
    'x_max' : 'xmax_1',
    'y_min' : 'ymin_1',
    'y_max' : 'ymax_1',
    'AreaKm' : 'AreaKm',

    'mpoly' : 'MULTIPOLYGON',
}

grilla_shp = os.path.abspath(
    os.path.join(os.path.dirname(__file__), 'shapes', 'grillapixeles2.shp'),
)

def run(verbose=True):
    lm = LayerMapping(Grilla, grilla_shp, grilla_mapping, transform=False)
    lm.save(strict=True, verbose=verbose)