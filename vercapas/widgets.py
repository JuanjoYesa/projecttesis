from django.forms import DateTimeInput, DateInput


class XDSoftDateTimePickerInput(DateTimeInput):
    template_name = 'xdsoft_datetimepicker.html'
