from django.urls import path
from django.conf.urls import url

from . import views

urlpatterns = [
	url(r'^$', views.index, name='index'),
	url(r'^pixeles_data/$', views.PixelesData, name = 'Pixeles_Data'),
	url(r'^subcuencas_data/$', views.SubCuencasData, name = 'SubCuencas_Data'),
	url(r'^rain_on_pixel/$', views.Rain_on_Pixel, name = 'Rain_on_Pixel'),
	url(r'^precipitacion/$', views.precipitacion, name='precipitacion'),
	url(r'^catchment/$', views.watercatchment, name='catchment'),
	url(r'^saritapronostic/$', views.saritapronostic, name='saritapronostic'),
    url(r'^fatimapronostic/$', views.fatimapronostic, name='fatimapronostic'),
    url(r'^ultimoregistro/$', views.last_update, name='last_update'),
    url(r'^saritacompare/$', views.saritacompare, name='saritacompare'),
    url(r'^fatimacompare/$', views.fatimacompare, name='fatimacompare'),
    url(r'^equipo/$', views.equipo, name='equipo'),
    url(r'^proyecto/$', views.proyecto, name='proyecto'),
    url(r'^faq/$', views.faq, name='faq'),
	path('value/', views.Precip_Data, name='value'),
	path('xdsoft-datetimepicker/', views.XDSoftDateTimePickerView, name='xdsoft'),
]