import os
from django.contrib.gis.utils import LayerMapping
from .models import SubCuencas

cuenca_mapping = {
	'id': 'ID',
    'comid' : 'COMID',
    'grid_code' : 'GRID_CODE',
    'grid_count' : 'GRID_COUNT',
    'prod_unit' : 'PROD_UNIT',
    'areasqkm' : 'AREASQKM',

    'mpoly' : 'MULTIPOLYGON',
}

cuenca_shp = os.path.abspath(
    os.path.join(os.path.dirname(__file__), 'shapes', 'SupPotiy.shp'),
)

def run(verbose=True):
    lm = LayerMapping(SubCuencas, cuenca_shp, cuenca_mapping, transform=False)
    lm.save(strict=True, verbose=verbose)