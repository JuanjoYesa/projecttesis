from django.contrib import admin
from django.contrib.gis import admin

from .models import Pixeles

admin.site.register(Pixeles, admin.OSMGeoAdmin)
