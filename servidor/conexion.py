#!/usr/bin/python3
from SaveToDB import RecuperarValoresBD, ObtenerUltimoIdPd, IdPdEnPronostico
import os
from datetime import datetime
from datetime import timedelta
from dateutil.relativedelta import relativedelta
import time
from pathlib import Path
from Clases import SessionWithHeaderRedirection, GuardarValoresHDF5
import requests

def descargar():

	ultimo_id_pd = ObtenerUltimoIdPd()
	lista_idpd_pronostico = IdPdEnPronostico()
	if ultimo_id_pd in lista_idpd_pronostico:
		
		username = "nankom"		 
		password= "Pokemon12345"
		session = SessionWithHeaderRedirection(username, password) # create session with the user credentials that will be used to authenticate access to the data

		day_of_year = time.localtime().tm_yday
		hoy = f"{datetime.now():%Y%m%d}"
		hora = f"{datetime.now():%H}"
		hora_ultimo_archivo = int(hora) - 2

		hora_fecha_actual = str(hora).zﬁll(2)+str(hoy)
		hora_fecha_ultimo_archivo = str(hora_ultimo_archivo).zﬁll(2)+str(hoy)

		dato = RecuperarValoresBD()
		print("Dato Recuperado: "+str(dato))
		if dato == []:
			print("Base de datos vacia")
			starting_year = datetime.now() - relativedelta(years=4)
			dayofyearbd = starting_year.timetuple()
			dayofyearbd = dayofyearbd.tm_yday
			fechabd = f"{starting_year:%Y%m%d}"
			anhobd =  f"{starting_year:%Y}"
			starthour = "000000"
			endhour = "002959"
			codebd = "0000"
			hora_fecha_bd = "00"+str(fechabd)
			controlador_starthour = "00"
		else:
			fechabd = f"{dato[0]:%Y%m%d}"
			anhobd =  f"{dato[0]:%Y}"  
			starthour = dato[1]
			endhour = dato[2]
			dayofyearbd = dato[4]
			horaBD = str(starthour[0])+str(starthour[1])
			minBD = str(starthour[2])+str(starthour[3])

			controlador = int(horaBD)*60 + int(minBD)
			hora_fecha_bd = str(horaBD)+str(fechabd)
			codebd = str(int(dato[3])+30).zﬁll(4)

			if hora_fecha_bd != hora_fecha_actual and int(controlador) < 1410:
				fechabd = f"{dato[0]:%Y%m%d}"
				anhobd =  f"{dato[0]:%Y}" 
			else:
				if hora_fecha_bd != hora_fecha_actual:
					fechabdOr = dato[0] + timedelta(days=1) 
					fechabd = f"{fechabdOr:%Y%m%d}"
					anhobd = f"{fechabdOr:%Y}"
					controlador_primer_dia = f"{fechabdOr:%m%d}"
					if(str(controlador_primer_dia) =="0101"): 
						dayofyearbd = 1
					else:
						dayofyearbd = int(dayofyearbd) +1
					codebd = "0000"

			controlador_starthour = str(starthour[2])+str(starthour[3])
			starthour_format = str(starthour[0])+str(starthour[1])+":"+str(starthour[2])+str(starthour[3])+":"+str(starthour[4])+str(starthour[5])
			starthour = (datetime.strptime(starthour_format, '%H:%M:%S') + timedelta(minutes=30)).time()
			starthour = f"{starthour:%H%M%S}"
			

			endhour_format = str(endhour[0])+str(endhour[1])+":"+str(endhour[2])+str(endhour[3])+":"+str(endhour[4])+str(endhour[5])
			endhour = (datetime.strptime(endhour_format, '%H:%M:%S') + timedelta(minutes=30)).time()
			endhour = f"{endhour:%H%M%S}"	
			
		dayofyearbd = str(dayofyearbd).zﬁll(3)
		urlbd = "https://gpm1.gesdisc.eosdis.nasa.gov/data/GPM_L3/GPM_3IMERGHHE.06/"+str(anhobd)+"/"+str(dayofyearbd)+"/3B-HHR-E.MS.MRG.3IMERG."+str(fechabd)+"-S"+str(starthour)+"-E"+str(endhour)+"."+str(codebd)+".V06B.HDF5"
		filename = "/home/IMERG/" + urlbd[urlbd.rfind('/')+1:]

		print("URL: "+str(urlbd))
		print("FILEPATH: "+str(filename))
		my_file_path = Path(filename)
		while 1:
			
			if hora_fecha_bd == hora_fecha_ultimo_archivo and int(controlador_starthour) == 30:
				print("Archivos al día. Esperando para iniciar nueva descarga.")
				os.system("timeout 600")
				break
			else:
				if not my_file_path.is_file():
					print("Archivo todavía no descargado:")
					print(str(filename))
					try:
					 
						response = session.get(urlbd, stream=True)
					 
						print(response.status_code)
					  
						response.raise_for_status()   
					 
						with open(filename, 'wb') as fd: # save the file
							for chunk in response.iter_content(chunk_size=1024*1024):
								fd.write(chunk)
						print("loquesea")
						GuardarValoresHDF5(my_file_path,filename, fechabd, starthour, endhour, codebd, dayofyearbd)
						os.remove(filename)
						break
					except requests.exceptions.HTTPError as e:

						print(e)
						print("Esperando para nuevo intento de descarga.")
						os.system("timeout 300")
						break

					except Exception as e:
						print("Something else went wrong")
						print(e)
						break
						
				else:
					print("Archivo descargado.")
					os.remove(filename)
					break
	else:
		print("Las Predicciones no estan al dia. Falta predecir archivo con id_pd: "+str(ultimo_id_pd))


try:
	descargar()
except  Exception as e:
	print("Hubo un error. "+str(e))