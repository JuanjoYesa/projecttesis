# Use numpy to convert to arrays
import numpy as np
from sklearn.model_selection import train_test_split
import matplotlib as mpl
import matplotlib.pyplot as plt
import os
import pandas as pd
from datetime import datetime, timedelta
from sklearn.ensemble import RandomForestRegressor
import pickle

def parser(x):
    return datetime.strptime(x,'%Y-%m-%d %H:%M:%S+00')

features = pd.read_csv('pixel9.csv',index_col=0, sep=';', parse_dates=[0], date_parser=parser)

mm = features['mm_h'].values
features["previous"] = np.nan

c = 0
for index, row in features.iterrows():
  if c != 0:
    #print(row['previous'])
    #print(mm[c-1])
    features.set_value(index, 'previous', mm[c-1])
    #print(row['previous'])
    #os.system("pause")
  c=c+1

features = features['2015-01-08 00:00:00':'2018-12-31 23:30:00'] #2018-03-31 23:30:00
features = features.dropna()
#print(features)
#indexes = features.index.values
#print(indexes)
#os.system("pause")
# Labels are the values we want to predict
labels = np.array(features['mm_h'])
# Remove the labels from the features
# axis 1 refers to the columns
features= features.drop('mm_h', axis = 1)
# Saving feature names for later use
feature_list = list(features.columns)
# Convert to numpy array
features = np.array(features)

#actual_dates = features[:,feature_list.index()]
#print(feature_list)
#os.system("pause")


# Using Skicit-learn to split data into training and testing sets

# Split the data into training and testing sets
train_features, test_features, train_labels, test_labels = train_test_split(features, labels, test_size = 0.25, random_state = 42)

# Instantiate model with 1000 decision trees
rf = RandomForestRegressor(n_estimators = 1000, random_state = 44)
# Train the model on training data
rf.fit(train_features, train_labels)


filename = 'modelo_rf_pixel9.sav'
pickle.dump(rf, open(filename, 'wb'))

