#!/usr/bin/python3
import requests
import h5py
import numpy as np
from SaveToDB import insertval
from pathlib import Path
import os

class SessionWithHeaderRedirection(requests.Session):
 
	AUTH_HOST = 'urs.earthdata.nasa.gov'
 
	def __init__(self, username, password):
 
		super().__init__()
 
		self.auth = (username, password)
 
  
 
   # Overrides from the library to keep headers when redirected to or from
 
   # the NASA auth host.
 
	def rebuild_auth(self, prepared_request, response):
 
		headers = prepared_request.headers
 
		url = prepared_request.url
 
  
 
		if 'Authorization' in headers:
 
			original_parsed = requests.utils.urlparse(response.request.url)
 
			redirect_parsed = requests.utils.urlparse(url)
 
  
 
			if (original_parsed.hostname != redirect_parsed.hostname) and redirect_parsed.hostname != self.AUTH_HOST and original_parsed.hostname != self.AUTH_HOST:
 
				del headers['Authorization']
 
  
 
		return


def GuardarValoresHDF5(my_file_path,filename, fecha, starthour, endhour, code, doyear):
	try:
		#ingresar el nombre del archivo que se debe leer
		#hdffile = "/home/IMERG/3B-HHR-E.MS.MRG.3IMERG."+str(fecha)+"-S"+str(starthour)+"-E"+str(endhour)+"."+str(code)+".V05B.hdf5"
		#hdffile = Path(hdffile)
		f = h5py.File(my_file_path, mode = 'r')
		#path to the shape
		shape = 'Grid/precipitationCal'
		#se obtiene el valor ingresando los index de la latitud y de la longitud
		#ej. longindex 1226 - latindex 622 
		pixel = []
		pixel.append(f[shape][:,1239,629]) #Valor del pixel 01
		pixel.append(f[shape][:,1240,629]) #Valor del pixel 02
		pixel.append(f[shape][:,1241,629]) #Valor del pixel 03
		pixel.append(f[shape][:,1242,629]) #Valor del pixel 04
		pixel.append(f[shape][:,1243,629]) #Valor del pixel 05
		pixel.append(f[shape][:,1244,629]) #Valor del pixel 06
		pixel.append(f[shape][:,1239,628]) #Valor del pixel 07
		pixel.append(f[shape][:,1240,628]) #Valor del pixel 08

		pixel.append(f[shape][:,1241,628]) #Valor del pixel 01 - 09 es cuenca
		pixel.append(f[shape][:,1242,628]) #Valor del pixel 02 - 10 es cuenca

		pixel.append(f[shape][:,1243,628]) #Valor del pixel 11
		pixel.append(f[shape][:,1244,628]) #Valor del pixel 12

		pixel.append(f[shape][:,1239,627]) #Valor del pixel 13
		pixel.append(f[shape][:,1240,627]) #Valor del pixel 14

		pixel.append(f[shape][:,1241,627]) #Valor del pixel 03 - 15 es cuenca
		pixel.append(f[shape][:,1242,627]) #Valor del pixel 04 - 16 es cuenca
		pixel.append(f[shape][:,1243,627]) #Valor del pixel 07 - 17 es cuenca

		pixel.append(f[shape][:,1244,627]) #Valor del pixel 18

		pixel.append(f[shape][:,1239,626]) #Valor del pixel 19
		pixel.append(f[shape][:,1240,626]) #Valor del pixel 20

		pixel.append(f[shape][:,1241,626]) #Valor del pixel 05 - 21
		pixel.append(f[shape][:,1242,626]) #Valor del pixel 06 - 22

		pixel.append(f[shape][:,1243,626]) #Valor del pixel 23
		pixel.append(f[shape][:,1244,626]) #Valor del pixel 24
		pixel.append(f[shape][:,1239,625]) #Valor del pixel 25
		pixel.append(f[shape][:,1240,625]) #Valor del pixel 26
		pixel.append(f[shape][:,1241,625]) #Valor del pixel 27
		pixel.append(f[shape][:,1242,625]) #Valor del pixel 28
		pixel.append(f[shape][:,1243,625]) #Valor del pixel 29
		pixel.append(f[shape][:,1244,625]) #Valor del pixel 30

		print('-------------------')

		fecha = str(fecha)+" "+ str(starthour)
		starthour = str(starthour)
		endhour = str(endhour)
		code = str(code)
		doyear = str(doyear)
		values_list = []
		#recorre los valores en pixel y env?a a la funci?n insertval para guardar en la bd
		print(pixel[0])

		for i, mm_h in enumerate(pixel):

			mm_h = float(mm_h)
			vol_total = float("{0:.2f}".format(110000000 * mm_h)) 

			#print('Valor en Pixel Nº ',i+1,': ', mm_h, ' mm/h \ncon un volumen total de ', vol_total, 'mm3/h\n')
			id_pix = i+1
			values = (id_pix, fecha, mm_h, vol_total)
 
			values_list.append(values)

		insertval(values_list, starthour, endhour, code, doyear)
		print('Datos guardados.')
		print('-------------------')

	except Exception as e:
		print("ERROR ES: ")
		print(e)
		os.remove(filename)
		


