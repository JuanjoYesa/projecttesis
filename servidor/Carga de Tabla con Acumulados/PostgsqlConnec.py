#!/usr/bin/python
import psycopg2
from config import config

#se conecta a la base de datos 
def connect():
    """ Connect to the PostgreSQL database server """
    conn = None
    try:
        # read connection parameters
        params = config()
 
        # connect to the PostgreSQL server
        print('Connecting to the PostgreSQL database...')
        conn = psycopg2.connect(**params)
        #curs = conn.cursor()
        return conn
        
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)

#cierra la conexión
def closeconnect(conex):
    if conex is not None:
        conex.close()
        print('Database connection closed.')
