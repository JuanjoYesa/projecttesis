import psycopg2
import psycopg2.extras
from datetime import datetime
from datetime import timedelta
from dateutil.relativedelta import relativedelta
import time
from PostgsqlConnec import connect, closeconnect

fecha = datetime.strptime('2015-01-08 00:00:00', '%Y-%m-%d %H:%M:%S')
fecha_fin = datetime.strptime('2019-12-14 14:00:00', '%Y-%m-%d %H:%M:%S')
#sql1 = "SELECT id_data_sub, fecha_subc, vol_subc, id_subc FROM public.data_hist_subc where (id_subc = 6) and (fecha_subc = %s)"
#Sql para estirar valores de los pixeles 10, 15 y 16
sql1 = """SELECT mm_h, id_pix FROM public.precip_data 
			where id_pix in (21,22) and (fecha = %s) order by id_pix"""
areasubc5 = 30 * 1000000 #convierte de Km2 a m2 el área 
#El acumulado se guarda en mm3

while fecha < fecha_fin:
	print ("La fecha en proceso es: %s", fecha)
	try:
	    conex = connect()
	    cur = conex.cursor()
	    strfecha = str(fecha)
	    # execute a statement
	    cur.execute(sql1, (strfecha,))
	    #volumen_en_cuenca = cur.fetchone()
	    pixeles = cur.fetchall()
	    lluvia_subc = float(pixeles[0][0])*0.6288 + float(pixeles[1][0])*0.3712
	    volumen_en_cuenca = float(lluvia_subc/1000) * areasubc5
	    print("Volumen del momento: ", volumen_en_cuenca)
	    cur.execute("SELECT vol_acum FROM public.data_vol_acum_subcuencas where id_subc = 6 order by fecha_subc desc limit 1")
	    ultimo_vol_acum = cur.fetchone()
	    print("Último Volumen Acumulado: ", ultimo_vol_acum)

	    if(ultimo_vol_acum == None):
	    	ultimo_acum = 0
	    else:
	    	ultimo_acum = float(ultimo_vol_acum[0])

	    acum_actual = float(ultimo_acum) + float(volumen_en_cuenca) - float(float(ultimo_acum)/8)
	    id_subc = 6
	    sql = """INSERT INTO data_vol_acum_subcuencas (fecha_subc, vol_acum, id_subc) VALUES(%s, %s, %s);"""
	    cur.execute(sql, (strfecha, acum_actual, id_subc,))
	    conex.commit()
	 # close the communication with the PostgreSQL
	    cur.close()
	    fecha = fecha + timedelta(minutes=30)
			
	except (Exception, psycopg2.DatabaseError) as error:
	    print(error)
	finally:
	    closeconnect(conex)