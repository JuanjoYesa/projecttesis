#!/usr/bin/python3
import requests
import urllib.request
import time
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
from pytz import timezone
import pytz
import psycopg2
import psycopg2.extras
from PostgsqlConnec import connect, closeconnect

def recuperarultimoregistro():
	try:
	    # read database 
		conex = connect()
	    # create a new cursor
		cur = conex.cursor()
			
		# execute a statement
		cur.execute("SELECT id_da, fecha_datmos, temperatura, humedad, presion FROM public.datosatmos ORDER BY fecha_datmos DESC LIMIT 1")
		result = cur.fetchall()
		print(result[0])
		return result[0]
	 
		cur.close()
	except(Exception, psycopg2.DatabaseError) as error:
		print(error)
	finally:
		closeconnect(conex)

def agregardatosAtmos(fecha_hora, temperatura, humedad, presionatm):
	try:
	    # read database 
		conex = connect()
	    # create a new cursor
		cur = conex.cursor()

		fecha_hora = fecha_hora.strftime("%Y-%m-%d %H:%M:%S")
		
		sql = """INSERT INTO public.datosatmos(fecha_datmos, temperatura, humedad, presion)VALUES(%s, %s, %s, %s);"""
		# execute a statement
		cur.execute(sql, (fecha_hora, temperatura, humedad, presionatm))
		conex.commit()
		print("Datos ingresados: ", fecha_hora, "\n",temperatura,"\n", humedad,"\n", presionatm)
		cur.close()
	except(Exception, psycopg2.DatabaseError) as error:
		print(error)
	finally:
		closeconnect(conex)

def actualizardatosAtmos(id_da, fecha_hora, temperatura, humedad, presionatm):
	try:
		conex = connect()
		cur = conex.cursor()
		fecha_hora = fecha_hora.strftime("%Y-%m-%d %H:%M:%S")
		sql = """ 
				UPDATE public.datosatmos
                SET fecha_datmos = %s,
                	temperatura = %s, 
                	humedad = %s, 
                	presion = %s
                WHERE id_da = %s
                """
		cur.execute(sql, (fecha_hora, temperatura, humedad, presionatm, id_da))
		conex.commit()
		print("Datos Actualizados: ", fecha_hora, "\n",temperatura,"\n", humedad,"\n", presionatm)
		cur.close()
	except(Exception, psycopg2.DatabaseError) as error:
		print(error)
	finally:
		closeconnect(conex)

consulta = recuperarultimoregistro()
id_ult_endb = consulta[0]
fechaendb = consulta[1]
tempendb = consulta[2]
humendb = consulta[3]
presendb = consulta[4]
print("Último Registro en BD: ", fechaendb)

url = 'https://posadas.gov.ar/opad/online/'
response = requests.get(url)
zonahoraria = timezone('America/Buenos_Aires')

soup = BeautifulSoup(response.text, 'html.parser')

aux = soup.findAll('table')[0].select("caption")[0].get_text() #para obtener los valores
print(aux)
aux = aux.split("local ")
aux = aux[1].split(" on ")
hora = aux[0] + ":00"
fecha = aux[1].replace(" ", "-").replace("enero", 
	"01").replace("febrero", "02").replace("marzo", "03").replace("abril", 
	"04").replace("mayo", "05").replace("junio", "06").replace("julio", 
	"07").replace("agosto", "08").replace("setiembre", "09").replace("septiembre", 
	"09").replace("octubre", "10").replace("noviembre", "11").replace("diciembre", "12")
fecha = fecha.split("-")
caption = fecha[2]+ "/" + fecha[1] + "/" + fecha[0] + " " + hora
fechapag = datetime.strptime(caption, '%Y/%m/%d %H:%M:%S')
#fechapag = zonahoraria.localize(fechapag)
print ("Hora local en pag: ", fechapag)
#fechapag = fechapag.astimezone(timezone('UTC')) #convertir hora local a UTC para comparación y posterior guardado en la Base de datos
fechapag = fechapag + timedelta(hours = 3) 
print("Hora local a UTC: ", fechapag)
minutoenpg = caption.split(":")
minutoenpg = minutoenpg[1]

def calcpromedio(uno, dos):
	promedio = (uno + dos)/2
	return promedio

if fechapag > fechaendb: #and (minutoenpg == "30" or minutoenpg == "00"):
	
	#Temperatura
	aux = soup.findAll('table')[0].select("td")[2].get_text() #para obtener los valores
	split = aux.split()
	temperatura = float(split[0].replace(",","."))
	
	#Humedad
	aux = soup.findAll('table')[0].select("td")[8].get_text() #para obtener los valores
	humedad = float(aux.replace("%",""))
	
	#Presión Atm.
	aux = soup.findAll('table')[0].select("td")[14].get_text() #para obtener los valores
	split = aux.split()
	presionatm = float(split[0].replace(",","."))
	
	print('Time at: ', fechapag, '\nTemperatura: ', temperatura,'\nHumedad: ', humedad, '\nPresión Atm.: ', presionatm)
	#print("minuto en DB: ", fechaendb.minute)
	if fechaendb.minute == 00:
		if fechapag == (fechaendb + timedelta(minutes = 10)) or fechapag == (fechaendb + timedelta(minutes = 20)) or fechapag == (fechaendb + timedelta(minutes = 30)):
			print("Entre 10 y 30")
			agregardatosAtmos(fechapag, temperatura, humedad, presionatm)
		elif fechapag == (fechaendb + timedelta(minutes = 40)) or fechapag == (fechaendb + timedelta(minutes = 50)) or fechapag == (fechaendb + timedelta(hours = 1)):
			#Se promedian los valores para insertar en registro faltante de 30min
			tempprom = (tempendb+temperatura)/2
			humedprom = (humendb+humedad)/2
			presionprom = (presendb+presionatm)/2
			fechanueva = fechaendb + timedelta(minutes = 30)
			agregardatosAtmos(fechanueva, tempprom, humedprom, presionprom)
			agregardatosAtmos(fechapag, temperatura, humedad, presionatm)
		else:
			agregardatosAtmos(fechapag, temperatura, humedad, presionatm)
	elif fechaendb.minute == 10:	
		if fechapag == (fechaendb + timedelta(minutes = 10)) or fechapag == (fechaendb + timedelta(minutes = 20)):
			actualizardatosAtmos(id_ult_endb, fechapag, temperatura, humedad, presionatm)
		elif fechapag == (fechaendb + timedelta(minutes = 30)) or fechapag == (fechaendb + timedelta(minutes = 40)) or fechapag == (fechaendb + timedelta(minutes = 50)):
			#Se promedian los valores para actualizar registro faltante de 30min
			tempprom = (tempendb+temperatura)/2
			humedprom = (humendb+humedad)/2
			presionprom = (presendb+presionatm)/2
			fechanueva = fechaendb + timedelta(minutes = 20)
			actualizardatosAtmos(id_ult_endb, fechanueva, tempprom, humedprom, presionprom)
			agregardatosAtmos(fechapag, temperatura, humedad, presionatm)
		else:
			agregardatosAtmos(fechapag, temperatura, humedad, presionatm)
	elif fechaendb.minute == 20:
		if fechapag == (fechaendb + timedelta(minutes = 10)):
			# = 30 min. se actualiza el registro
			actualizardatosAtmos(id_ult_endb, fechapag, temperatura, humedad, presionatm)
		elif fechapag == (fechaendb + timedelta(minutes = 20)): 
			# = 40 min.
			#Se promedian los valores para actualizar registro faltante de 30min
			tempprom = (tempendb+temperatura)/2
			humedprom = (humendb+humedad)/2
			presionprom = (presendb+presionatm)/2
			fechanueva = fechaendb + timedelta(minutes = 10)
			actualizardatosAtmos(id_ult_endb, fechanueva, tempprom, humedprom, presionprom)
			agregardatosAtmos(fechapag, temperatura, humedad, presionatm)
		elif fechapag == (fechaendb + timedelta(minutes = 30)) or fechapag == (fechaendb + timedelta(minutes = 40)):
			# = 50 min o 00 min. - Guardar valor de 20 min. en registro de 30 min.
			fechanueva = fechaendb + timedelta(minutes = 10)
			actualizardatosAtmos(id_ult_endb, fechanueva, tempendb, humendb, presendb)
			agregardatosAtmos(fechapag, temperatura, humedad, presionatm)
		else:
			agregardatosAtmos(fechapag, temperatura, humedad, presionatm)
	elif fechaendb.minute == 30:
		if fechapag == (fechaendb + timedelta(minutes = 40)) or fechapag == (fechaendb + timedelta(minutes = 50)) or fechapag == (fechaendb + timedelta(hours = 1)):
			# = 10 min. o 20 min. o 30 min. 
			#Se promedian los valores para actualizar registro faltante de 00min
			print("mayor a 00 hasta 30 de la siguiente hora")
			tempprom = (tempendb+temperatura)/2
			humedprom = (humendb+humedad)/2
			presionprom = (presendb+presionatm)/2
			fechanueva = fechaendb + timedelta(minutes = 30)
			agregardatosAtmos(fechanueva, tempprom, humedprom, presionprom)
			agregardatosAtmos(fechapag, temperatura, humedad, presionatm)
		else:
			print("Entre 40 y 00")
			agregardatosAtmos(fechapag, temperatura, humedad, presionatm)
	elif fechaendb.minute == 40:
		if fechapag == (fechaendb + timedelta(minutes = 10)) or fechapag == (fechaendb + timedelta(minutes = 20)):
			# = 50 min. o = 00 min. actualiza los valores del registro
			print("Entre 50 y 00")
			actualizardatosAtmos(id_ult_endb, fechapag, tempendb, humendb, presendb)
		elif fechapag == (fechaendb + timedelta(minutes = 30)):
			# = 10 min. actualizar valores en registro de 00
			print("Igual a 10")
			fechanueva = fechaendb + timedelta(minutes = 20)
			actualizardatosAtmos(id_ult_endb, fechanueva, temperatura, humedad, presionatm)
		elif fechapag == (fechaendb + timedelta(minutes = 40)) or fechapag == (fechaendb + timedelta(minutes = 50)):
			# = 20 min. o = 30 min. Promediar, actualizar registro a 00 e insertar nuevo registro
			#Se promedian los valores para actualizar registro faltante de 30min
			print("Entre 20 y 30")
			tempprom = (tempendb+temperatura)/2
			humedprom = (humendb+humedad)/2
			presionprom = (presendb+presionatm)/2
			fechanueva = fechaendb + timedelta(minutes = 20)
			actualizardatosAtmos(id_ult_endb, fechanueva, tempprom, humedprom, presionprom)
			agregardatosAtmos(fechapag, temperatura, humedad, presionatm)
		else:
			agregardatosAtmos(fechapag, temperatura, humedad, presionatm)
	else:
		if fechapag == (fechaendb + timedelta(minutes = 10)):
			# = 00 min. se actualiza registro a 00 min.
			print("Igual a 00")
			actualizardatosAtmos(id_ult_endb, fechapag, temperatura, humedad, presionatm)
		elif fechapag == (fechaendb + timedelta(minutes = 20)):
			# = 10 min. se promedian los valores para actualizar registro faltante de 00 min.
			print("Igual a 10")
			tempprom = (tempendb+temperatura)/2
			humedprom = (humendb+humedad)/2
			presionprom = (presendb+presionatm)/2
			fechanueva = fechaendb + timedelta(minutes = 10)
			actualizardatosAtmos(id_ult_endb, fechanueva, tempprom, humedprom, presionprom)
		elif fechapag == (fechaendb + timedelta(minutes = 30)) or fechapag == (fechaendb + timedelta(minutes = 40)):
			# = 20 o = 30 Promediar valores para actualizar registros en faltante de 00 min. e insertar siguiente registro
			print("Entre 20 y 30")
			tempprom = (tempendb+temperatura)/2
			humedprom = (humendb+humedad)/2
			presionprom = (presendb+presionatm)/2
			fechanueva = fechaendb + timedelta(minutes = 10)
			actualizardatosAtmos(id_ult_endb, fechanueva, tempprom, humedprom, presionprom)
			agregardatosAtmos(fechapag, temperatura, humedad, presionatm)
		else:
			# = 40 o más - Promediar valores para actualizar registros en faltante de 00 min. e insertar siguiente registro
			print("Entre 40 y más")
			tempprom = (tempendb+temperatura)/2
			humedprom = (humendb+humedad)/2
			presionprom = (presendb+presionatm)/2
			fechanueva = fechaendb + timedelta(minutes = 10)
			actualizardatosAtmos(id_ult_endb, fechanueva, tempprom, humedprom, presionprom)
			fechanueva = fechaendb + timedelta(minutes = 30)
			agregardatosAtmos(fechanueva, tempprom, humedprom, presionprom)
			agregardatosAtmos(fechapag, temperatura, humedad, presionatm)
		#actualizardatosAtmos(id_ult_endb, fechanueva, tempprom, humedprom, presionprom)
		#agregardatosAtmos(fechapag, temperatura, humedad, presionatm)
else:
	print ("Fecha ya registrada o registro fuera de minutos aceptados")