#!/usr/bin/python3
import psycopg2
import psycopg2.extras
from PostgsqlConnec import connect, closeconnect
import os

def insertval(values_list, starthour, endhour, code, doyear):
	
	def hist_segsubc(lista_segsubc_x_pixel, fecha_hora, mm_h):
		
		lista_calculo_cuantificacion_x_seg_de_pixel = []
		for fila in lista_segsubc_x_pixel:
			area_en_milimetros = float(fila[3]) * 1000000000000 #pasamos area de metros cuadrados a milimetros cuadrados
			volumen_segmento = area_en_milimetros * float(mm_h)
			valores = (fila[0], fecha_hora, volumen_segmento, fila[3])   
			lista_calculo_cuantificacion_x_seg_de_pixel.append(valores)
			
		
		return lista_calculo_cuantificacion_x_seg_de_pixel
		
		
	""" insert a new vendor into the vendors table """
	sql = """INSERT INTO data_vol_acum_subcuencas (fecha_subc, vol_acum, id_subc) VALUES(%s, %s, %s);""" #guardar valores acumulados de SubCuencas
	    
	sql1 = """INSERT INTO datos_desc(starthour, endhour, code, dofyear)
				VALUES (%s, %s, %s, %s) RETURNING id_arch;"""
	sql2 = """INSERT INTO precip_data(id_pix, id_arch,fecha, mm_h, vol_total) 
				VALUES %s"""
	sql3 = """INSERT INTO data_hist_segsubc(id_seg, fecha_segsubc, vol_segm) 
				VALUES %s"""
	sql4 = """INSERT INTO data_hist_subc(id_subc, fecha_subc, vol_subc) 
				VALUES %s"""
				
	try:
        # read database 
		conex = connect()
        # create a new cursor
		cur = conex.cursor()
		cur.execute(sql1, (starthour, endhour, code, doyear,)) #Inserta los registros de los datos del archivo descargado SQL1
		id_arch = cur.fetchone()[0]
		lista_precip_data = []
		for x in values_list:
			x = list(x)
			x.insert(1, int(id_arch))
			x = tuple(x)
			lista_precip_data.append(x)
		lista_hist_segsubc = []
		lista_temporal_hist_segsubc = []
		volumen_segmento_subcuenca = [0, 0, 0, 0, 0, 0]
		lista_historico_subcuena = []
		for x in lista_precip_data: 
			fecha_hora_archivo = x[2]
			lista_result_histo_segsubc = []
			#lista2 = []
			if x[0] == 9:
				lista_segsubc_x_pixel = Cuantificacion(x[0],x[2])
				lista_result_histo_segsubc.extend(hist_segsubc(lista_segsubc_x_pixel, x[2],x[3]))
			if x[0] == 10:
				lista_segsubc_x_pixel = Cuantificacion(x[0],x[2])
				lista_result_histo_segsubc.extend(hist_segsubc(lista_segsubc_x_pixel, x[2],x[3]))
			if x[0] == 15:
				lista_segsubc_x_pixel = Cuantificacion(x[0],x[2])
				lista_result_histo_segsubc.extend(hist_segsubc(lista_segsubc_x_pixel, x[2],x[3]))
			if x[0] == 16:
				lista_segsubc_x_pixel = Cuantificacion(x[0],x[2])
				lista_result_histo_segsubc.extend(hist_segsubc(lista_segsubc_x_pixel, x[2],x[3]))
			if x[0] == 17:
				lista_segsubc_x_pixel = Cuantificacion(x[0],x[2])
				lista_result_histo_segsubc.extend(hist_segsubc(lista_segsubc_x_pixel, x[2],x[3]))
			if x[0] == 21:
				lista_segsubc_x_pixel = Cuantificacion(x[0],x[2])
				lista_result_histo_segsubc.extend(hist_segsubc(lista_segsubc_x_pixel, x[2],x[3]))
			if x[0] == 22:
				lista_segsubc_x_pixel = Cuantificacion(x[0],x[2])
				lista_result_histo_segsubc.extend(hist_segsubc(lista_segsubc_x_pixel, x[2],x[3]))
				
			lista_temporal_hist_segsubc.extend(lista_result_histo_segsubc)
				
		for y in lista_temporal_hist_segsubc:
			valores_hist_segsubc = (y[0], y[1], y[2])
			lista_hist_segsubc.append(valores_hist_segsubc)
			
			if y[3] == 1:
				volumen_segmento_subcuenca[0] = float(volumen_segmento_subcuenca[0]) + float(y[2])
			if y[3] == 2:
				volumen_segmento_subcuenca[1] = float(volumen_segmento_subcuenca[1]) + float(y[2])
			if y[3] == 3:
				volumen_segmento_subcuenca[2] = float(volumen_segmento_subcuenca[2]) + float(y[2])
			if y[3] == 4:
				volumen_segmento_subcuenca[3] = float(volumen_segmento_subcuenca[3]) + float(y[2])
			if y[3] == 5:
				volumen_segmento_subcuenca[4] = float(volumen_segmento_subcuenca[4]) + float(y[2])
			if y[3] == 6:
				volumen_segmento_subcuenca[5] = float(volumen_segmento_subcuenca[5]) + float(y[2])
				
			
		for idx, yy in enumerate(volumen_segmento_subcuenca):
			valores_hist_subc = ((int(idx)+1),fecha_hora_archivo, volumen_segmento_subcuenca[idx])
			lista_historico_subcuena.append(valores_hist_subc)
			
		for ya in lista_hist_segsubc:
			print("Lista Historico SEG subcuenca: "+str(ya))
			
		for yay in lista_historico_subcuena:
			print("Lista Historico subcuenca: "+str(yay))
		
		#acumulado subcuenca 6 fatima potiy VER PARA UNIFICAR LOS CÓDIGOS DE ACUMULACIÓN DE LAS SUBCUENCAS
		cur.execute("SELECT fecha_subc, vol_acum, id_subc FROM public.data_vol_acum_subcuencas where id_subc = 6 order by fecha_subc desc limit 1")
		ultimo_vol_acum = cur.fetchone()

		if(ultimo_vol_acum == None):
			ultimo_acum = 0
		else:
			ultimo_acum = float(ultimo_vol_acum[1])

		acum_actual = float(ultimo_acum) + float(volumen_segmento_subcuenca[5]) - float(float(ultimo_acum)/8)
		id_subc_seis = 6
		cur.execute(sql, (fecha_hora_archivo, acum_actual, id_subc_seis,))

		#acumulado subcuenca 5 sarita pora
		cur.execute("SELECT fecha_subc, vol_acum, id_subc FROM public.data_vol_acum_subcuencas where id_subc = 5 order by fecha_subc desc limit 1")
		ultimo_vol_acum = cur.fetchone()

		if(ultimo_vol_acum == None):
			ultimo_acum = 0
		else:
			ultimo_acum = float(ultimo_vol_acum[1])
		#para 8hs. Sarita tarda más en descargar
		acum_actual = float(ultimo_acum) + float(volumen_segmento_subcuenca[4]) - float(float(ultimo_acum)/16) 
		id_subc_cinco = 5
		cur.execute(sql, (fecha_hora_archivo, acum_actual, id_subc_cinco,))

		#execute the INSERT statement

		psycopg2.extras.execute_values(cur, sql2, lista_precip_data)
		psycopg2.extras.execute_values(cur, sql3, lista_hist_segsubc)
		psycopg2.extras.execute_values(cur, sql4, lista_historico_subcuena)
		#commit the changes to the database
		conex.commit()
		#close communication with the database
		cur.close()
	except (Exception, psycopg2.DatabaseError) as error:
		print(error)
	finally:
		closeconnect(conex)


def RecuperarValoresBD():

    try:
        conex = connect()
        cur = conex.cursor()
        
        # execute a statement
        cur.execute("SELECT precip_data.fecha, datos_desc.starthour, datos_desc.endhour, datos_desc.code, datos_desc.dofyear FROM public.precip_data INNER JOIN public.datos_desc ON precip_data.id_arch = datos_desc.id_arch ORDER BY fecha DESC, id_pd DESC")
 
        #display the PostgreSQL database server version
        db_version = cur.fetchall()
        #db_version = cur.fetchall()
        '''
        for row in db_version:
           db_version = row
        '''
        
        return db_version[0]
     # close the communication with the PostgreSQL
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        closeconnect(conex)


def ObtenerVariablesAtmosfericas(fecha):

    try:
        fecha = fecha.strftime('%Y-%m-%d %H:%M:%S')
        conex = connect()
        cur = conex.cursor()

        cur.execute("SELECT datosatmos.fecha_datmos, datosatmos.temperatura, datosatmos.humedad, datosatmos.presion FROM public.datosatmos WHERE fecha_datmos > '"+str(fecha)+"' ORDER BY fecha_datmos ASC, id_da DESC")

        db_version = cur.fetchall()

        return db_version
        cur.close()

    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        closeconnect(conex)
	
def Cuantificacion(id_pixel, mm_h):
	id_pixel = str(id_pixel)
	sql1 = """SELECT id_seg, area_cobxpix, id_pix, id_subc FROM public.seg_cuencas WHERE public.seg_cuencas.id_pix = %s"""

	try:
		conex = connect()
		cur = conex.cursor()
        
		# execute a statement
		cur.execute(sql1,[id_pixel])

		#display the PostgreSQL database server version
		db_version = cur.fetchall()
		#db_version = cur.fetchall()
		values_list = []
		for row in db_version:
			values_list.append(row)
		
		# close the communication with the PostgreSQL
		cur.close()
		return values_list
	except (Exception, psycopg2.DatabaseError) as error:
		print(error)
	finally:
		closeconnect(conex)


def ObtenerVariablesAtmosfericas_manual(fecha_inicio, fecha_fin):

    try:
        conex = connect()
        cur = conex.cursor()

        cur.execute("SELECT datosatmos.fecha_datmos, datosatmos.temperatura, datosatmos.humedad, datosatmos.presion FROM public.datosatmos WHERE fecha_datmos >= '"+str(fecha_inicio)+"' AND fecha_datmos <= '"+str(fecha_fin)+"'  ORDER BY fecha_datmos ASC, id_da ASC")

        db_version = cur.fetchall()

        return db_version
        cur.close()

    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        closeconnect(conex)


def VariablesAtmosfericasToPredictReales(fecha_inicio, fecha_fin):

    try:
        conex = connect()
        cur = conex.cursor()

        cur.execute("SELECT datosatmos.fecha_datmos, datosatmos.temperatura, datosatmos.humedad, datosatmos.presion FROM public.datosatmos WHERE fecha_datmos >= '"+str(fecha_inicio)+"' AND fecha_datmos < '"+str(fecha_fin)+"'  ORDER BY fecha_datmos ASC, id_da ASC")

        db_version = cur.fetchall()

        return db_version
        cur.close()

    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        closeconnect(conex)


def ObtenerUltimasLluviasParaPrediccionRandomForest(fecha_inicio, fecha_fin, pixel):
	
	try:
		#fecha_inicio = fecha_inicio.strftime('%Y-%m-%d %H:%M:%S')
		conex = connect()
		cur = conex.cursor()
		#precip_data.fecha,
		cur.execute("SELECT precip_data.mm_h FROM public.precip_data WHERE precip_data.id_pix = '"+str(pixel)+"' AND precip_data.fecha >= '"+str(fecha_inicio)+"' AND precip_data.fecha < '"+str(fecha_fin)+"'ORDER BY fecha ASC, id_pd ASC")

		db_version = cur.fetchall()

		return db_version
		cur.close()

	except (Exception, psycopg2.DatabaseError) as error:
		print(error)
	finally:
		closeconnect(conex)	        


def ObtenerIDarchivo(fecha_hora):
	
	try:
		conex = connect()
		cur = conex.cursor()

		cur.execute("SELECT precip_data.id_pd FROM public.precip_data WHERE  precip_data.fecha = '"+str(fecha_hora)+"' ORDER BY fecha ASC, id_pd ASC")

		db_version = cur.fetchall()

		return db_version[0]
		cur.close()

	except (Exception, psycopg2.DatabaseError) as error:
		print(error)
	finally:
		closeconnect(conex)

def ObtenerIDarchivoPorPixel(fecha_hora, pixel):
	
	try:
		conex = connect()
		cur = conex.cursor()

		cur.execute("SELECT precip_data.id_pd FROM public.precip_data WHERE  precip_data.fecha = '"+str(fecha_hora)+"' AND id_pix = '"+str(pixel)+"' ORDER BY fecha ASC, id_pd ASC")

		db_version = cur.fetchall()

		return db_version[0]
		cur.close()

	except (Exception, psycopg2.DatabaseError) as error:
		print(error)
	finally:
		closeconnect(conex)


def GuardarPredicciones(lista_datmos_a_guardar, lista_mmh_a_guardar):

	sql1 = """INSERT INTO pronost_datmos(valor_ptemp, valor_phumd, valor_ppresion, id_pd, fecha_pdatmos) 
				VALUES %s"""
	sql2 = """INSERT INTO pronost_precip(valor_pprecip, fecha_pprecip, id_pd) 
				VALUES %s"""
	
	try:
		conex = connect()
		cur = conex.cursor()

		#execute the INSERT statement

		psycopg2.extras.execute_values(cur, sql1, lista_datmos_a_guardar)
		psycopg2.extras.execute_values(cur, sql2, lista_mmh_a_guardar)
		
		#commit the changes to the database
		conex.commit()
		#close communication with the database
		cur.close()

	except (Exception, psycopg2.DatabaseError) as error:
		print(error)
	finally:
		closeconnect(conex)	 



def ObtenerUltimoIdPd():
	
	try:
		conex = connect()
		cur = conex.cursor()

		cur.execute("SELECT precip_data.id_pd FROM public.precip_data ORDER BY id_pd DESC Limit 1")

		db_version = cur.fetchall()

		return db_version[0]
		cur.close()

	except (Exception, psycopg2.DatabaseError) as error:
		print(error)
	finally:
		closeconnect(conex)


def IdPdEnPronostico():
	
	try:
		conex = connect()
		cur = conex.cursor()

		cur.execute("SELECT pronost_datmos.id_pd FROM public.pronost_datmos ORDER BY id_pd DESC")

		db_version = cur.fetchall()

		return db_version
		cur.close()

	except (Exception, psycopg2.DatabaseError) as error:
		print(error)
	finally:
		closeconnect(conex)

def ObtenerHoraIdPd(id_pd):
	
	try:
		conex = connect()
		cur = conex.cursor()

		cur.execute("SELECT precip_data.fecha FROM public.precip_data WHERE precip_data.id_pd = "+str(id_pd)+" ORDER BY id_pd DESC Limit 1")

		db_version = cur.fetchall()

		return db_version[0]
		cur.close()

	except (Exception, psycopg2.DatabaseError) as error:
		print(error)
	finally:
		closeconnect(conex)