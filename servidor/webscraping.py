#!/usr/bin/python3
import requests
import urllib.request
import time
from datetime import datetime
from bs4 import BeautifulSoup
from pytz import timezone
import pytz
import psycopg2
import psycopg2.extras
from PostgsqlConnec import connect, closeconnect

def recuperarultimoregistro():
	try:
	    # read database 
		conex = connect()
	    # create a new cursor
		cur = conex.cursor()
			
		# execute a statement
		cur.execute("SELECT fecha_datmos FROM public.datosatmos ORDER BY fecha_datmos DESC LIMIT 1")
		result = cur.fetchone()
		#ultimoregistro = result[0].strftime('%Y/%m/%d %H:%M:%S')
		#ultimoregistro = ultimoregistro.replace("/", "-")
		#print("Ultimo registro en BD: ", ultimoregistro)

		return result[0]
	 
		cur.close()
	except(Exception, psycopg2.DatabaseError) as error:
		print(error)
	finally:
		closeconnect(conex)

def agregardatosAtmos(fecha_hora, temperatura, humedad, presionatm):
	try:
	    # read database 
		conex = connect()
	    # create a new cursor
		cur = conex.cursor()

		fecha_hora = fecha_hora.astimezone(pytz.utc)
		fecha_hora = fecha_hora.strftime ("%Y-%m-%d %H:%M:%S")
		
		sql = """INSERT INTO public.datosatmos(fecha_datmos, temperatura, humedad, presion)VALUES(%s, %s, %s, %s);"""
		# execute a statement
		cur.execute(sql, (fecha_hora, temperatura, humedad, presionatm))
		conex.commit()
		print("Datos ingresados: ", fecha_hora, "\n",temperatura,"\n", humedad,"\n", presionatm)

		cur.close()
	except(Exception, psycopg2.DatabaseError) as error:
		print(error)
	finally:
		closeconnect(conex)

fechaendb = recuperarultimoregistro()

#url = 'http://clima.encarnacion.gov.py/estacion/index.htm'
url = 'https://www.meteorologia.gov.py/emas/'
response = requests.get(url)
#paraguay = timezone('America/Asuncion')

soup = BeautifulSoup(response.text, 'html.parser')
'''
aux = soup.findAll('table')[1].select("caption")[0].get_text() #para obtener los valores
aux = aux.split("time ")
aux = aux[1].split(" on ")
hora = aux[0] + ":00"
fecha = aux[1].replace(" ", "-").replace("enero", 
	"01").replace("febrero", "02").replace("marzo", "03").replace("abril", 
	"04").replace("mayo", "05").replace("junio", "06").replace("julio", 
	"07").replace("agosto", "08").replace("setiembre", "09").replace("septiembre", 
	"09").replace("octubre", "10").replace("noviembre", "11").replace("diciembre", "12")
fecha = fecha.split("-")
caption = fecha[2]+ "/" + fecha[1] + "/" + fecha[0] + " " + hora
fechapag = datetime.strptime(caption, '%Y/%m/%d %H:%M:%S')
fechapag = paraguay.localize(fechapag)
fechapag = fechapag.astimezone(timezone('UTC')) #convertir hora local a UTC para comparación y posterior guardado en la Base de datos

minutoenpg = caption.split(":")
minutoenpg = minutoenpg[1]
'''

ema = soup.findAll('table')[0].select("td")[432].get_text() #para obtener los valores
fechapag = soup.findAll('table')[0].select("td")[435].get_text() 
fechapag = datetime.strptime(fecha, "%d-%m-%Y %H:%M")
#temperatura = soup.findAll('table')[0].select("td")[436].get_text()
#humedad = soup.findAll('table')[0].select("td")[437].get_text()
#presionatm = soup.findAll('table')[0].select("td")[438].get_text()

if fechapag > fechaendb and (minutoenpg == "30" or minutoenpg == "00"):
	
	#Temperatura
	#aux = soup.findAll('table')[1].select("td")[2].get_text() #para obtener los valores
	#split = aux.split()
	#temperatura = float(split[0].replace(",","."))
	temperatura = float(soup.findAll('table')[0].select("td")[436].get_text())
	
	#Punto de rocío
	#aux = soup.findAll('table')[1].select("td")[4].get_text() #para obtener los valores
	#split = aux.split()
	#puntorocio = float(split[0].replace(",","."))
	
	#Humedad
	#aux = soup.findAll('table')[1].select("td")[8].get_text() #para obtener los valores
	#humedad = float(aux.replace("%",""))
	humedad = float(soup.findAll('table')[0].select("td")[437].get_text())
	
	#Presión Atm.
	#aux = soup.findAll('table')[1].select("td")[41].get_text() #para obtener los valores
	#split = aux.split()
	#presionatm = float(split[0].replace(",","."))
	presionatm = float(soup.findAll('table')[0].select("td")[438].get_text())
	
	print('Time at: ', fechapag, '\nTemperatura: ', temperatura,'\nHumedad: ', humedad, '\nPresión Atm.: ', presionatm)
	agregardatosAtmos(fechapag, temperatura, humedad, presionatm)

else:
	print ("Fecha ya en registrada o registro correspondiente a 15 o 45 minutos")