import pandas as pd
from fbprophet import Prophet
import pickle
from datetime import datetime
import pytz
import csv
import numpy as np
from sklearn.model_selection import train_test_split
import matplotlib as mpl
import matplotlib.pyplot as plt
import os
import pendulum
from datetime import datetime, timedelta
from sklearn.ensemble import RandomForestRegressor
from SaveToDB import ObtenerUltimasLluviasParaPrediccionRandomForest, ObtenerVariablesAtmosfericas_manual, VariablesAtmosfericasToPredictReales, ObtenerIDarchivo, GuardarPredicciones, ObtenerIDarchivoPorPixel, ObtenerUltimoIdPd, ObtenerHoraIdPd, IdPdEnPronostico


# Function to insert row in the dataframe 
def Insert_row(row_number, df, row_value): 
    # Starting value of upper half 
    start_upper = 0

    # End value of upper half 
    end_upper = row_number 
   
    # Start value of lower half 
    start_lower = row_number 
   
    # End value of lower half 
    end_lower = df.shape[0] 
   
    # Create a list of upper_half index 
    upper_half = [*range(start_upper, end_upper, 1)] 
   
    # Create a list of lower_half index 
    lower_half = [*range(start_lower, end_lower, 1)] 
   
    # Increment the value of lower half by 1 
    lower_half = [x.__add__(1) for x in lower_half] 
   
    # Combine the two lists 
    index_ = upper_half + lower_half 
   
    # Update the index of the dataframe 
    df.index = index_ 
   
    # Insert a row at the end 
    df.loc[row_number] = row_value 
    
    # Sort the index labels 
    df = df.sort_index() 
   
    # return the dataframe 
    return df 

def unificar(prediccion1, prediccion2):
	aux = 1
	for index, row in prediccion2.iterrows():
		if(index<prediccion2.index.stop): 
			prediccion1 = Insert_row((index+aux), prediccion1, row)
			aux = aux + 1 	
	return prediccion1


ultimo_id_pd = ObtenerUltimoIdPd()
lista_idpd_pronostico = IdPdEnPronostico()

if ultimo_id_pd not in lista_idpd_pronostico:
	fecha_hora_archivo = ObtenerHoraIdPd(ultimo_id_pd[0])
	fecha_hora_archivo = pendulum.instance(fecha_hora_archivo[0])
	Hora = datetime.now()
	print("HORA INICIO SISTEMA: "+str(Hora))

	fecha_hora_inicio = fecha_hora_archivo.subtract(hours=1)
	#fecha_mediahora_inicio = datetime.strptime("2019-01-04 15:00:00",'%Y-%m-%d %H:%M:%S')

	print("FECHA HORA ARCHIVO:")
	print(fecha_hora_archivo)

	cantidad_horas_a_predecir_reales = 0
	tz = pytz.timezone('America/Asuncion') # obtener timezone de paraguay
	hora_paraguaya = datetime.now(tz)
	hora_paraguaya_naive = hora_paraguaya.replace(tzinfo=None)
	id_archivo = ultimo_id_pd[0]

	fecha_fin = fecha_hora_inicio

	diferencia_hora = datetime.utcnow()-hora_paraguaya_naive #se obtiene la cantidad en horas de la diferencia horaria para saber la cantidad de horas a predecir con los datos reales
	segundos = diferencia_hora.seconds
	cantidad_diferencia_hora = round(segundos / 3600)

	if(cantidad_diferencia_hora==3):     #fix cambio de hora
		retraso_hora_archivo = 2
	else:
		if(cantidad_diferencia_hora==4):
			retraso_hora_archivo = 1

	cantidad_horas_a_predecir_reales = cantidad_diferencia_hora + retraso_hora_archivo + 1 #+1 porque hay que comenzar que se setea como timestamp de inicio una hora antes del archivo obtenido para hacer la prediccion
	print("CANTIDAD HORAS A PREDECR REALES: "+str(cantidad_horas_a_predecir_reales))
	hora_inicio_to_predict_reales = fecha_fin.subtract(hours=1)
	hora_fin_to_predict_reales = fecha_fin.add(hours=cantidad_horas_a_predecir_reales)
	print("HORA FIN TO PREDICT REALES: "+str(hora_fin_to_predict_reales))

	fecha_inicio_prediccion_atmosfericos_irreales = hora_fin_to_predict_reales.subtract(hours=1)
	fecha_media_inicio_prediccion_atmosfericos_irreales = hora_fin_to_predict_reales.subtract(minutes=30)
	fecha_fin_redondo = fecha_inicio_prediccion_atmosfericos_irreales 

	ValoresAPredecir = pd.DataFrame.from_records(VariablesAtmosfericasToPredictReales(hora_inicio_to_predict_reales, hora_fin_to_predict_reales))
	ValoresAPredecir.columns = ['fecha','temperatura','humedad','presion']

	print(ValoresAPredecir)

	fecha_inicio = fecha_inicio_prediccion_atmosfericos_irreales.subtract(years=1)
	fecha_inicio = fecha_inicio.strftime('%Y-%m-%d %H:%M:%S')
	fecha_inicio_prediccion_atmosfericos_irreales = fecha_inicio_prediccion_atmosfericos_irreales.strftime('%Y-%m-%d %H:%M:%S')
	
	fecha_fin_media = pendulum.instance(fecha_media_inicio_prediccion_atmosfericos_irreales)

	hora_predecida = []
	hora_a_sumar = fecha_fin_media.add(minutes=30)
	for i in range(6):
	    hora_a_guardar = hora_a_sumar.strftime('%Y-%m-%d %H:%M:%S')
	    hora_predecida.append(hora_a_guardar)
	    hora_a_sumar = hora_a_sumar.add(minutes=30)


	fecha_inicio_media = fecha_fin_media.subtract(years=1)
	fecha_inicio_media = fecha_inicio_media.strftime('%Y-%m-%d %H:%M:%S')
	fecha_fin_media = fecha_fin_media.strftime('%Y-%m-%d %H:%M:%S')

	valores_atmosfericos_redondo = ObtenerVariablesAtmosfericas_manual(fecha_inicio,fecha_fin_redondo)
	df = pd.DataFrame.from_records(valores_atmosfericos_redondo)
	df.columns = ['fecha','temperatura','humedad','presion']

	valores_atmosfericos_media = ObtenerVariablesAtmosfericas_manual(fecha_inicio_media,fecha_fin_media)
	mdf = pd.DataFrame.from_records(valores_atmosfericos_media)
	mdf.columns = ['fecha','temperatura','humedad','presion']

	temperatura = df[['fecha','temperatura']].copy()
	temperatura.columns = ['ds','y']
	temperatura['ds'] = temperatura['ds'].dt.tz_convert(None)


	mtemperatura = mdf[['fecha','temperatura']].copy()
	mtemperatura.columns = ['ds','y']
	mtemperatura['ds'] = mtemperatura['ds'].dt.tz_convert(None)

	humedad = df[['fecha','humedad']].copy()
	humedad.columns = ['ds','y']
	humedad['ds'] = humedad['ds'].dt.tz_convert(None)

	mhumedad = mdf[['fecha','humedad']].copy()
	mhumedad.columns = ['ds','y']
	mhumedad['ds'] = mhumedad['ds'].dt.tz_convert(None)    

	presion = df[['fecha','presion']].copy()
	presion.columns = ['ds','y']
	presion['ds'] = presion['ds'].dt.tz_convert(None)

	mpresion = mdf[['fecha','presion']].copy()
	mpresion.columns = ['ds','y']
	mpresion['ds'] = mpresion['ds'].dt.tz_convert(None)

	Hora = datetime.now()
	print("TEMPERATURA")
	print("HORA INICIO: "+str(Hora))

	t = Prophet(yearly_seasonality=True)
	t.fit(temperatura)
	# 3 es el numero correcto para predecir en el sistema
	future = t.make_future_dataframe(periods=3, freq='H') #
	fcst_t = t.predict(future)

	Hora = datetime.now()
	print("HORA FIN: "+str(Hora))


	Hora = datetime.now()
	print("TEMPERATURA HALF")
	print("HORA INICIO: "+str(Hora))

	mt = Prophet(yearly_seasonality=True)
	mt.fit(mtemperatura)

	mfuture = mt.make_future_dataframe(periods=3, freq='H') 
	mfcst_t = mt.predict(mfuture)

	Hora = datetime.now()
	print("HORA FIN: "+str(Hora))


	Hora = datetime.now()
	print("HUMEDAD")
	print("HORA INICIO: "+str(Hora))

	h = Prophet(yearly_seasonality=True)
	h.fit(humedad)

	future = h.make_future_dataframe(periods=3, freq='H') 
	fcst_h = h.predict(future)

	Hora = datetime.now()
	print("HORA FIN: "+str(Hora))


	Hora = datetime.now()
	print("HUMEDAD HALF")
	print("HORA INICIO: "+str(Hora))

	mh = Prophet(yearly_seasonality=True)
	mh.fit(mhumedad)

	mfuture = mh.make_future_dataframe(periods=3, freq='H') #
	mfcst_h = mh.predict(mfuture)

	Hora = datetime.now()
	print("HORA FIN: "+str(Hora))


	Hora = datetime.now()
	print("PRESION")
	print("HORA INICIO: "+str(Hora))

	p = Prophet(yearly_seasonality=True)
	p.fit(presion)

	future = p.make_future_dataframe(periods=3, freq='H') #
	fcst_p = p.predict(future)

	Hora = datetime.now()
	print("HORA FIN: "+str(Hora))

	Hora = datetime.now()
	print("PRESION HALF")
	print("HORA INICIO: "+str(Hora))

	mp = Prophet(yearly_seasonality=True)
	mp.fit(mpresion)

	mfuture = mp.make_future_dataframe(periods=3, freq='H') #
	mfcst_p = mp.predict(mfuture)

	Hora = datetime.now()
	print("HORA FIN: "+str(Hora))


	fcst_t = fcst_t.tail(3)
	fcst_t = fcst_t.reset_index(inplace=False, drop = True)

	mfcst_t = mfcst_t.tail(3)
	mfcst_t = mfcst_t.reset_index(inplace=False, drop = True)

	prediccion_t = unificar(fcst_t, mfcst_t)


	fcst_h = fcst_h.tail(3)
	fcst_h = fcst_h.reset_index(inplace=False, drop = True)

	mfcst_h = mfcst_h.tail(3)
	mfcst_h = mfcst_h.reset_index(inplace=False, drop = True)

	prediccion_h = unificar(fcst_h, mfcst_h)

	fcst_p = fcst_p.tail(3)
	fcst_p = fcst_p.reset_index(inplace=False, drop = True)

	mfcst_p = mfcst_p.tail(3)
	mfcst_p = mfcst_p.reset_index(inplace=False, drop = True)

	prediccion_p = unificar(fcst_p, mfcst_p)

	Predicciones = pd.concat([prediccion_t['yhat'], prediccion_h['yhat'], prediccion_p['yhat']], axis=1, keys=['temperatura', 'humedad', 'presion']) 

	Hora = datetime.now()
	print("HORA FIN SISTEMA: "+str(Hora))
	hora_fin_dataframe_pixel = ValoresAPredecir['fecha'].iloc[5] #para que se utilicen los datos de lluvia registradas hasta el momento como "lluvia anterior"

	print("VALORES A PREDECIR valores_datmos_a_guardar------------------------------------------------")
	valores_datmos_a_guardar = pd.DataFrame()
	valores_datmos_a_guardar = Predicciones

	indice_archivo_datmos_predichos = []
	lista_valores_datmos_a_guardar = []

	for row in valores_datmos_a_guardar['humedad']:
	    indice_archivo_datmos_predichos.append(id_archivo)

	valores_datmos_a_guardar['id_pd'] = indice_archivo_datmos_predichos
	valores_datmos_a_guardar['fecha'] = hora_predecida
	valores_a_guardar_mmh = valores_datmos_a_guardar.values
	lista_valores_datmos_a_guardar = valores_a_guardar_mmh
	print(valores_datmos_a_guardar)

	print("--------------------------------------------------------------")

	fecha_df = ValoresAPredecir['fecha'].copy()
	fecha_para_predecir_con_lluvia_anterior = fecha_df.iloc[:6]
	fecha_para_unir_con_datos_irreales = fecha_df.iloc[6:]
	valores_datmos_a_guardar = valores_datmos_a_guardar.drop(columns=['id_pd'])
	valores_datmos_a_guardar = valores_datmos_a_guardar.drop(columns=['fecha'])
	Predicciones = Predicciones.drop(columns=['id_pd'])
	Predicciones = Predicciones.drop(columns=['fecha'])
	ValoresAPredecir = ValoresAPredecir.drop(columns=['fecha'])
	print(ValoresAPredecir)

	array_mmh_guardar = []
	lista_valores_mmh_a_guardar = []
	pixeles = [9,10,15,16,17,21,22] 
	for x in pixeles:
	    dataframe_para_pixel = pd.DataFrame()
	    dataframe_para_pixel  = ValoresAPredecir
	    dataframe_para_predecir_con_lluvia_anterior = dataframe_para_pixel.iloc[:6]
	    dataframe_para_unir_con_datos_irreales = dataframe_para_pixel.iloc[6:]
	    
	    
	    lista = []
	    print("FECHA HORA INICIO PIXEL "+str(x))
	    hora_inicio_lluvia_anterior = hora_inicio_to_predict_reales.subtract(minutes=30)
	    
	    lista_ultima_lluvia_para_prediccion = ObtenerUltimasLluviasParaPrediccionRandomForest(hora_inicio_lluvia_anterior,hora_fin_dataframe_pixel,x)
	    lista_ultima_lluvia_para_prediccion = np.array(lista_ultima_lluvia_para_prediccion)
	    
	    

	    dataframe_para_predecir_con_lluvia_anterior['mm_h_anterior'] = lista_ultima_lluvia_para_prediccion
	    print(dataframe_para_predecir_con_lluvia_anterior)
	    print("---------------------------------------------")
	    print(dataframe_para_unir_con_datos_irreales)
	    
	    datos_atmosfericos_reales_para_predecir = dataframe_para_predecir_con_lluvia_anterior.values #se cargan los valores reales para realizar la prediccion

	    Hora = datetime.now()
	    print("HORA INICIO PREDICCION "+str(x)+": "+str(Hora))

	    filename = 'modelo_rf_pixel'+str(x)+'.sav'
	    rf = pickle.load(open(filename, 'rb'))

	    lista_predicciones_datos_reales = []

	    print(datos_atmosfericos_reales_para_predecir)
	    print("################################################################")
	    for i in range(len(datos_atmosfericos_reales_para_predecir)):
	      prediction = rf.predict(datos_atmosfericos_reales_para_predecir[i].reshape(1,-1))
	      lista_predicciones_datos_reales.append(prediction)


	    
	    ultimo_valor_de_lluvia = lista_predicciones_datos_reales[-1]
	    lista_predicciones_datos_reales = pd.DataFrame.from_records(lista_predicciones_datos_reales)
	    lista_predicciones_datos_reales['fecha'] = fecha_para_predecir_con_lluvia_anterior
	    
	    print("------------------------------------")#Se prepara la columna de fecha hora de las variables que faltan predecir
	    hora_para_irreales = pd.DataFrame()
	    hora_para_irreales[0] = hora_predecida
	    fecha_irreales = fecha_para_unir_con_datos_irreales.append(hora_para_irreales, ignore_index=True)
	    valores_de_horas = fecha_irreales.values
	    valores_de_horas = valores_de_horas.flat
	    

	    lista_predicciones_datos_predichos = []
	    unificacion_datos_reales_con_irreales = dataframe_para_unir_con_datos_irreales.append(Predicciones, ignore_index=True)
	    valores_prediccion_fb = unificacion_datos_reales_con_irreales.values
	    array_valores_prediccion = np.array(valores_prediccion_fb)
	    helper_column = np.zeros((14,1))
	    array_datos_prediccion_fb_helper = np.hstack((array_valores_prediccion,helper_column))
	    
	    
	    for i in range(len(array_datos_prediccion_fb_helper)):
	      array_datos_prediccion_fb_helper[i][3] = ultimo_valor_de_lluvia
	      prediction = rf.predict(array_datos_prediccion_fb_helper[i].reshape(1,-1))
	      ultimo_valor_de_lluvia = prediction
	      lista_predicciones_datos_predichos.append(prediction)

	    lista_predicciones_datos_predichos = pd.DataFrame.from_records(lista_predicciones_datos_predichos)
	    lista_predicciones_datos_predichos['fecha'] = valores_de_horas

	    prediccion_final = lista_predicciones_datos_reales.append(lista_predicciones_datos_predichos, ignore_index=True)

	    #hora_prediccion = fecha_hora_inicio+ timedelta(minutes=60) 

	    #-----------------------------------------------------------------------
	    indice_archivo_reales = []
	    id_archivo_por_pixel = ObtenerIDarchivoPorPixel(fecha_hora_archivo,x)
	    for row in prediccion_final['fecha']:
	        indice_archivo_reales.append(id_archivo_por_pixel)
	    prediccion_final['id_pd'] = indice_archivo_reales
	    valores_a_guardar_mmh = prediccion_final.values
	    
	    array_mmh_guardar.extend(valores_a_guardar_mmh)

	    #----------------------------------------------------------------------
	    Hora = datetime.now()
	    print("HORA FIN: "+str(Hora))

	lista_valores_mmh_a_guardar = pd.DataFrame(array_mmh_guardar)

	print("LISTA valores_a_guardar_mmh -----------------------------------------")
	lista_valores_mmh_a_guardar = lista_valores_mmh_a_guardar.values
	print(lista_valores_mmh_a_guardar)
	print("----------------------------------------------------------------------")
	GuardarPredicciones(lista_valores_datmos_a_guardar, lista_valores_mmh_a_guardar)

else:
	print("Predicciones al dia con archivo con id_pd: "+str(ultimo_id_pd))