#!/usr/bin/python3
import requests
import urllib.request
import time
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
from pytz import timezone
import pytz
import psycopg2
import psycopg2.extras
from PostgsqlConnec import connect, closeconnect

def recuperarultimoregistro():
	try:
	    # read database 
		conex = connect()
	    # create a new cursor
		cur = conex.cursor()
			
		# execute a statement
		cur.execute("SELECT fecha_datmos FROM public.datosatmos ORDER BY fecha_datmos DESC LIMIT 1")
		result = cur.fetchone()
		#ultimoregistro = result[0].strftime('%Y/%m/%d %H:%M:%S')
		#ultimoregistro = ultimoregistro.replace("/", "-")
		#print("Ultimo registro en BD: ", ultimoregistro)

		return result[0]
	 
		cur.close()
	except(Exception, psycopg2.DatabaseError) as error:
		print(error)
	finally:
		closeconnect(conex)

def agregardatosAtmos(fecha_hora, temperatura, humedad, presionatm):
	try:
	    # read database 
		conex = connect()
	    # create a new cursor
		cur = conex.cursor()

		fecha_hora = fecha_hora.strftime("%Y-%m-%d %H:%M:%S")
		
		sql = """INSERT INTO public.datosatmos(fecha_datmos, temperatura, humedad, presion)VALUES(%s, %s, %s, %s);"""
		# execute a statement
		cur.execute(sql, (fecha_hora, temperatura, humedad, presionatm))
		conex.commit()
		print("Datos ingresados: ", fecha_hora, "\n",temperatura,"\n", humedad,"\n", presionatm)
		cur.close()
	except(Exception, psycopg2.DatabaseError) as error:
		print(error)
	finally:
		closeconnect(conex)

fechaendb = recuperarultimoregistro()

url = 'https://posadas.gov.ar/opad/online/'
response = requests.get(url)
zonahoraria = timezone('America/Buenos_Aires')

soup = BeautifulSoup(response.text, 'html.parser')

aux = soup.findAll('table')[0].select("caption")[0].get_text() #para obtener los valores
print(aux)
aux = aux.split("local ")
aux = aux[1].split(" on ")
hora = aux[0] + ":00"
fecha = aux[1].replace(" ", "-").replace("enero", 
	"01").replace("febrero", "02").replace("marzo", "03").replace("abril", 
	"04").replace("mayo", "05").replace("junio", "06").replace("julio", 
	"07").replace("agosto", "08").replace("setiembre", "09").replace("septiembre", 
	"09").replace("octubre", "10").replace("noviembre", "11").replace("diciembre", "12")
fecha = fecha.split("-")
caption = fecha[2]+ "/" + fecha[1] + "/" + fecha[0] + " " + hora
fechapag = datetime.strptime(caption, '%Y/%m/%d %H:%M:%S')
#fechapag = zonahoraria.localize(fechapag)
print ("Hora local en pag: ", fechapag)
#fechapag = fechapag.astimezone(timezone('UTC')) #convertir hora local a UTC para comparación y posterior guardado en la Base de datos
fechapag = fechapag + timedelta(hours = 3) 
print("Hora local a UTC: ", fechapag)
minutoenpg = caption.split(":")
minutoenpg = minutoenpg[1]


if fechapag > fechaendb:# and (minutoenpg == "30" or minutoenpg == "00"):
	
	#Temperatura
	aux = soup.findAll('table')[0].select("td")[2].get_text() #para obtener los valores
	split = aux.split()
	temperatura = float(split[0].replace(",","."))
	
	#Humedad
	aux = soup.findAll('table')[0].select("td")[8].get_text() #para obtener los valores
	humedad = float(aux.replace("%",""))
	
	#Presión Atm.
	aux = soup.findAll('table')[0].select("td")[14].get_text() #para obtener los valores
	split = aux.split()
	presionatm = float(split[0].replace(",","."))
	
	print('Time at: ', fechapag, '\nTemperatura: ', temperatura,'\nHumedad: ', humedad, '\nPresión Atm.: ', presionatm)
	agregardatosAtmos(fechapag, temperatura, humedad, presionatm)

else:
	print ("Fecha ya registrada o registro fuera de minutos aceptados")