from django.db import models
from django.contrib.gis.db import models

class PixelesManager(models.Manager):
    def get_by_natural_key(self, x_min, x_max, y_min, y_max, indx_lat, indx_lon, es_mbc, areakm):
        return self.get(x_min=x_min, x_max=x_max, y_min=y_min, y_max=y_max, indx_lat=indx_lat, indx_lon=indx_lon, es_mbc=es_mbc, areakm=areakm)

class Pixeles(models.Model):
    objects = PixelesManager()
    #---Atributos del Shape-------
    id_pix = models.BigIntegerField(primary_key=True)
    x_min = models.FloatField()
    x_max = models.FloatField()
    y_min = models.FloatField()
    y_max = models.FloatField()
    #---Atributos de lat y lon-------
    indx_lat = models.IntegerField(null=True)
    indx_lon = models.IntegerField(null=True)
    #--Boolean si afecta o no a la cuenca
    es_mbc = models.BooleanField(default=False)
    
    areakm = models.BigIntegerField()
    
    mpoly = models.MultiPolygonField(srid=4326)

    def __unicode__(self):
        return self.areakm

    class Meta:
        db_table= 'pixeles'

    def natural_key(self):
        return(self.x_min,self.x_max,self.y_min,self.y_max,self.indx_lon,self.indx_lat, 
            self.es_mbc, self.areakm, self.mpoly)


class DatosDesc(models.Model):
    id_arch = models.AutoField(primary_key=True)
    starthour = models.TextField(blank=True, null=True)
    endhour = models.TextField(blank=True, null=True)
    code = models.TextField(blank=True, null=True)
    dofyear = models.TextField(blank=True, null=True)

    class Meta:
        db_table= 'datos_desc'

class PrecipData(models.Model):
    id_pd = models.AutoField(primary_key=True)
    id_pix = models.ForeignKey(Pixeles, models.DO_NOTHING, db_column='id_pix')
    id_arch = models.ForeignKey(DatosDesc, models.DO_NOTHING, db_column='id_arch')
    fecha = models.DateTimeField(blank=True, null=True)
    mm_h = models.FloatField(blank=True, null=True)
    vol_total = models.FloatField(blank=True, null=True)
    presion = models.FloatField(blank=True, null=True)

    class Meta:
        db_table= 'precip_data'

    def __string__(self):
        return self.fecha

    def natural_key(self):
        return self.pixeles.natural_key() + (self.mm_h,)

class SubCuencas(models.Model):
    comid = models.CharField(max_length = 20)
    grid_code = models.CharField(max_length = 20)
    grid_count = models.CharField(max_length = 20)
    prod_unit = models.CharField(max_length = 20)
    areasqkm = models.FloatField()
    mpoly = models.MultiPolygonField(srid=4326)

    def __unicode__(self):
        return self.comid
    class Meta:
        db_table= 'sub_cuencas'

class SegCuencas(models.Model):
    id_seg = models.AutoField(primary_key=True)
    id_pix = models.ForeignKey(Pixeles, models.DO_NOTHING, db_column='id_pix', blank=True, null=True)
    id_subc = models.ForeignKey('SubCuencas', models.DO_NOTHING, db_column='id_subc')
    area_cobxpix = models.FloatField(blank=True, null=True)

    class Meta:
        db_table= 'seg_cuencas'


class DataHistSegsubc(models.Model):
    id_datsegm = models.AutoField(primary_key=True)
    id_seg = models.ForeignKey('SegCuencas', models.DO_NOTHING, db_column='id_seg')
    fecha_segsubc = models.DateTimeField(blank=True, null=True)
    vol_segm = models.FloatField(blank=True, null=True)
    
    class Meta:
        db_table= 'data_hist_segsubc'

class DataVolAcumSubcuencas(models.Model):
    id_datavolacum = models.AutoField(primary_key=True)
    id_subc = models.ForeignKey('SubCuencas', models.DO_NOTHING, db_column='id_subc')
    fecha_subc = models.DateTimeField(blank=True,null=True)
    vol_acum = models.FloatField(blank=True, null=True)

    class Meta:
        db_table= 'data_vol_acum_subcuencas'

class DataHistSubc(models.Model):
    id_data_sub = models.AutoField(primary_key=True)
    id_subc = models.ForeignKey('SubCuencas', models.DO_NOTHING, db_column='id_subc')
    fecha_subc = models.DateTimeField(blank=True, null=True)
    vol_subc = models.FloatField(blank=True, null=True)
    
    class Meta:
        db_table= 'data_hist_subc'