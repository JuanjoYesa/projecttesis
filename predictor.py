import pandas as pd
from fbprophet import Prophet
import pickle
from datetime import datetime
from sklearn.metrics import mean_squared_error, r2_score, mean_absolute_error
import csv


df = pd.read_csv('pixel1.csv')
#df = df[21286:55879] #2 anhos
df = df[35346:52548]  #3332
#df = df[38586:55879] #1 anho
df_completo = df[0:55879]



temperatura = df[['fecha','temperatura']].copy()
temperatura.columns = ['ds','y']
print(temperatura)



humedad = df[['fecha','humedad']].copy()
humedad.columns = ['ds','y']
print(humedad)


punto_rocio = df[['fecha','punto_rocio']].copy()
punto_rocio.columns = ['ds','y']
print(punto_rocio)


presion = df[['fecha','presion']].copy()
presion.columns = ['ds','y']
print(presion)


Hora = datetime.now()
print("TEMPERATURA")
print("HORA INICIO: "+str(Hora))

t = Prophet(yearly_seasonality=True)#yearly_seasonality=True
t.fit(temperatura)

future = t.make_future_dataframe(periods=24, freq='H') #
fcst_t = t.predict(future)

print(fcst_t.tail(24))

Hora = datetime.now()
print("HORA FIN: "+str(Hora))



Hora = datetime.now()
print("HUMEDAD")
print("HORA INICIO: "+str(Hora))

h = Prophet(yearly_seasonality=True)#yearly_seasonality=True
h.fit(humedad)

future = h.make_future_dataframe(periods=24, freq='H') #
fcst_h = h.predict(future)
print(fcst_h.tail(24))
Hora = datetime.now()
print("HORA FIN: "+str(Hora))


Hora = datetime.now()
print("PUNTO ROCIO")
print("HORA INICIO: "+str(Hora))

r = Prophet(yearly_seasonality=True)#yearly_seasonality=True
r.fit(punto_rocio)

future = r.make_future_dataframe(periods=24, freq='H') #
fcst_r = r.predict(future)
print(fcst_r.tail(24))

Hora = datetime.now()
print("HORA FIN: "+str(Hora))


Hora = datetime.now()
print("PRESION")
print("HORA INICIO: "+str(Hora))

p = Prophet(yearly_seasonality=True)#yearly_seasonality=True
p.fit(presion)

future = p.make_future_dataframe(periods=24, freq='H') #
fcst_p = p.predict(future)
print(fcst_p.tail(24))
Hora = datetime.now()
print("HORA FIN: "+str(Hora))


metric_df = fcst_p.set_index('ds')[['yhat']].join(presion.set_index('ds').y).reset_index()
	
metric_df = metric_df.dropna()

print(metric_df)

print("R2 Score: "+str(r2_score(metric_df.y, metric_df.yhat)))
print("MSE: "+str(mean_squared_error(metric_df.y, metric_df.yhat)))
print("MAE: "+str(mean_absolute_error(metric_df.y, metric_df.yhat)))


fcst_t = fcst_t.tail(24)
fcst_h = fcst_h.tail(24)
fcst_r = fcst_r.tail(24)
fcst_p = fcst_p.tail(24)

Predicciones = pd.concat([fcst_t['yhat'], fcst_h['yhat'], fcst_r['yhat'], fcst_p['yhat']], axis=1, keys=['Temperatura', 'Humedad', 'Punto Rocio', 'Presion'])

#print(Predicciones)

# Use numpy to convert to arrays
import numpy as np
from sklearn.model_selection import train_test_split
import matplotlib as mpl
import matplotlib.pyplot as plt
import os
import pandas as pd
from datetime import datetime, timedelta
from sklearn.ensemble import RandomForestRegressor

def parser(x):
    return datetime.strptime(x,'%Y-%m-%d %H:%M:%S')

features = pd.read_csv('pixel1.csv',index_col=0, parse_dates=[0], date_parser=parser)

mm = features['mm_h'].values
features["previous"] = np.nan

c = 0
for index, row in features.iterrows():
  if c != 0:
    #print(row['previous'])
    #print(mm[c-1])
    features.set_value(index, 'previous', mm[c-1])
    #print(row['previous'])
    #os.system("pause")
  c=c+1

features = features['2015-01-08 00:00:00':'2018-03-31 23:30:00']
features = features.dropna()
#print(features)
#indexes = features.index.values
#print(indexes)
#os.system("pause")
# Labels are the values we want to predict
labels = np.array(features['mm_h'])
# Remove the labels from the features
# axis 1 refers to the columns
features= features.drop('mm_h', axis = 1)
# Saving feature names for later use
feature_list = list(features.columns)
# Convert to numpy array
features = np.array(features)

#actual_dates = features[:,feature_list.index()]
#print(feature_list)
#os.system("pause")


# Using Skicit-learn to split data into training and testing sets

# Split the data into training and testing sets
train_features, test_features, train_labels, test_labels = train_test_split(features, labels, test_size = 0.25, random_state = 42)

# Instantiate model with 1000 decision trees
rf = RandomForestRegressor(n_estimators = 1000, random_state = 42)
# Train the model on training data
rf.fit(train_features, train_labels)

# Use the forest's predict method on the test data
#predictions = rf.predict(test_features)
previous_rain = 0







#predictions = rf.predict(array.reshape(1,-1))
#print(predictions)



ejemplo = Predicciones.values
#print(type(ejemplo))
array = np.array(ejemplo)
helper_column = np.zeros((24,1))
datos = np.hstack((array,helper_column))
lista_predicciones = []
#print(datos)
previous = 0
for i in range(len(datos)):
  datos[i][4] = previous
  prediction = rf.predict(datos[i].reshape(1,-1))
  #print(datos[i])
  #print('Prediccion: '+str(prediction))
  previous = prediction
  lista_predicciones.append(prediction)


print(lista_predicciones)

with open('predicciones_prophet', 'w', newline='') as myfile:
     wr = csv.writer(myfile, quoting=csv.QUOTE_ALL)
     wr.writerow(lista_predicciones)