import pandas as pd
import matplotlib.pylab as plt
from numpy import array
from numpy import split
from math import sqrt
from pandas import read_csv
from sklearn.metrics import mean_squared_error
from matplotlib import pyplot
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Flatten
from keras.layers import LSTM


# split a univariate dataset into train/test sets
def split_dataset(data):
	# split into standard weeks
	train, test = data[:34752], data[34753:] #final sabado 2017 /52224
	# restructure into windows of weekly data
	print(len(train))
	print(train.shape)
	print(test.shape)
	print("----------------------------------------------------")

	train = array(split(train, len(train)/336))
	test = array(split(test, len(test)/336))
	return train, test


# evaluate one or more weekly forecasts against expected values
def evaluate_forecasts(actual, predicted):
	scores = list()
	# calculate an RMSE score for each day
	for i in range(actual.shape[1]):
		# calculate mse
		mse = mean_squared_error(actual[:, i], predicted[:, i])
		# calculate rmse
		rmse = sqrt(mse)
		# store
		scores.append(rmse)
	# calculate overall RMSE
	s = 0
	for row in range(actual.shape[0]):
		for col in range(actual.shape[1]):
			s += (actual[row, col] - predicted[row, col])**2
	score = sqrt(s / (actual.shape[0] * actual.shape[1]))
	return score, scores
 
# summarize scores
def summarize_scores(name, score, scores):
	s_scores = ', '.join(['%.1f' % s for s in scores])
	print('%s: [%.3f] %s' % (name, score, s_scores))
 
# convert history into inputs and outputs
def to_supervised(train, n_input, n_out=48):
	# flatten data
	data = train.reshape((train.shape[0]*train.shape[1], train.shape[2]))
	X, y = list(), list()
	in_start = 0
	# step over the entire history one time step at a time
	for _ in range(len(data)):
		# define the end of the input sequence
		in_end = in_start + n_input
		out_end = in_end + n_out
		# ensure we have enough data for this instance
		if out_end < len(data):
			x_input = data[in_start:in_end, 0]
			x_input = x_input.reshape((len(x_input), 1))
			X.append(x_input)
			y.append(data[in_end:out_end, 0])
		# move along one time step
		in_start += 1
	return array(X), array(y)


# train the model
def build_model(train, n_input):
	# prepare data
	train_x, train_y = to_supervised(train, n_input)
	# define parameters
	verbose, epochs, batch_size = 0, 70, 16
	n_timesteps, n_features, n_outputs = train_x.shape[1], train_x.shape[2], train_y.shape[1]
	# define model
	model = Sequential()
	model.add(LSTM(200, activation='relu', input_shape=(n_timesteps, n_features)))
	model.add(Dense(100, activation='relu'))
	model.add(Dense(n_outputs))
	model.compile(loss='mse', optimizer='adam')
	# fit network
	model.fit(train_x, train_y, epochs=epochs, batch_size=batch_size, verbose=verbose)
	return model
 
# make a forecast
def forecast(model, history, n_input):
	# flatten data
	data = array(history)
	data = data.reshape((data.shape[0]*data.shape[1], data.shape[2]))
	# retrieve last observations for input data
	input_x = data[-n_input:, 0]
	# reshape into [1, n_input, 1]
	input_x = input_x.reshape((1, len(input_x), 1))
	# forecast the next week
	yhat = model.predict(input_x, verbose=0)
	# we only want the vector forecast
	yhat = yhat[0]
	return yhat
 
# evaluate a single model
def evaluate_model(train, test, n_input):
	# fit model
	model = build_model(train, n_input)
	# history is a list of weekly data
	history = [x for x in train]
	# walk-forward validation over each week
	predictions = list()
	for i in range(len(test)):
		# predict the week
		yhat_sequence = forecast(model, history, n_input)
		# store the predictions
		predictions.append(yhat_sequence)
		# get real observation and add to history for predicting the next week
		history.append(test[i, :])
	# evaluate predictions days for each week
	predictions = array(predictions)
	score, scores = evaluate_forecasts(test[:, :, 0], predictions)
	return score, scores

print("Lectura CSV")
pd.set_option('display.precision',2)

df_precip = pd.read_csv('pixel1.csv', header = None, sep=';', index_col=0)

#df = pd.concat([df_precip[2], df_precip[6]], axis=1, keys=['mm_h', 'presion'])
#df_precip = df_precip['2015-01-11 00:30:00':'2017-12-31 00:00:00'] DATO COMPLETO PARA PREDECIR
df_precip = df_precip['2015-01-11 00:30:00':'2015-05-31 00:00:00']

df_precip[1] = df_precip[1].astype('float32')

print(df_precip.info())
'''
#df_precip[1] = df_precip[1].round(2)

print(df_precip)

train = df_precip['2015-01-11 00:30:00':'2015-03-29 00:00:00']
test = df_precip['2015-03-29 00:30:00':'2015-05-31 00:00:00']
print(len(train)/(48*7))
print(len(test)/(48*7))
print("*****************************************")

train = array(split(train.values, len(train)/(48*7)))
test = array(split(test.values, len(test)/(48*7)))
print(train.shape)
print(test.shape)
print("*****************************************")

n_input = 336
print("Evaluacion")
score, scores = evaluate_model(train, test, n_input)
# summarize scores
print("summarize")
summarize_scores('lstm', score, scores)
# plot scores
hours = ['0', '0.3', '1', '1.3', '2', '2.3', '3', '3.3', '4', '4.3', '5', '5.3', '6', '6.3', '7', '7.3', '8', '8.3', '9', '9.3', '10', '10.3', '11', '11.3', '12', '12.3', '13', '13.3','14', '14.3', '15', '15.3', '16', '16.3', '17', '17.3', '18', '18.3', '19', '19.3', '20', '20.3', '21', '21.3', '22', '22.3', '23', '23.3']
pyplot.plot(hours, scores, marker='o', label='lstm')
pyplot.show()

#print(df_precip)
#train, test = split_dataset(df_precip.values)
#print(train)
#print(test)
#print(df.index.min())
#print(df.index.max())

#print(df.describe())
#df['presion'].plot()
#plt.show()
'''